
def cl_gen():
    a = []
    def _to_ret():
        a.append(1)
        return a
    return _to_ret

s1 = cl_gen()
s2 = cl_gen()

print( s2() )
print( s2() )
print( s2() )
print( s1() )
print( s1() )

