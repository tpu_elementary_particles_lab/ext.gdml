# ./binding.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:e92452c8d3e28a9e27abfc9994d2007779e7f4c9
# Generated 2016-10-05 17:48:00.288754 by PyXB version 1.2.5 using Python 2.7.10.final.0
# Namespace AbsentNamespace0

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six
# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:2f0bf97e-8ae9-11e6-8464-040051161f9a')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.5'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# A holder for module-level binding classes so we can access them from
# inside class definitions where property names may conflict.
_module_typeBindings = pyxb.utils.utility.Object()

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.CreateAbsentNamespace()
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Complex type customer with content type ELEMENT_ONLY
class customer (pyxb.binding.basis.complexTypeDefinition):
    """Complex type customer with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'customer')
    _XSDLocation = pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 10, 0)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element firstname uses Python identifier firstname
    __firstname = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'firstname'), 'firstname', '__AbsentNamespace0_customer_firstname', False, pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 12, 4), )

    
    firstname = property(__firstname.value, __firstname.set, None, None)

    
    # Element lastname uses Python identifier lastname
    __lastname = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'lastname'), 'lastname', '__AbsentNamespace0_customer_lastname', False, pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 13, 4), )

    
    lastname = property(__lastname.value, __lastname.set, None, None)

    
    # Element country uses Python identifier country
    __country = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, 'country'), 'country', '__AbsentNamespace0_customer_country', False, pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 14, 4), )

    
    country = property(__country.value, __country.set, None, None)

    _ElementMap.update({
        __firstname.name() : __firstname,
        __lastname.name() : __lastname,
        __country.name() : __country
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.customer = customer
Namespace.addCategoryObject('typeBinding', 'customer', customer)


# Complex type Norwegian_customer with content type ELEMENT_ONLY
class Norwegian_customer (customer):
    """Complex type Norwegian_customer with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Norwegian_customer')
    _XSDLocation = pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 18, 0)
    _ElementMap = customer._ElementMap.copy()
    _AttributeMap = customer._AttributeMap.copy()
    # Base type is customer
    
    # Element firstname (firstname) inherited from customer
    
    # Element lastname (lastname) inherited from customer
    
    # Element country (country) inherited from customer
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Norwegian_customer = Norwegian_customer
Namespace.addCategoryObject('typeBinding', 'Norwegian_customer', Norwegian_customer)




customer._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'firstname'), pyxb.binding.datatypes.string, scope=customer, location=pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 12, 4)))

customer._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'lastname'), pyxb.binding.datatypes.string, scope=customer, location=pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 13, 4)))

customer._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, 'country'), pyxb.binding.datatypes.string, scope=customer, location=pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 14, 4)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(customer._UseForTag(pyxb.namespace.ExpandedName(None, 'firstname')), pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 12, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(customer._UseForTag(pyxb.namespace.ExpandedName(None, 'lastname')), pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 13, 4))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(customer._UseForTag(pyxb.namespace.ExpandedName(None, 'country')), pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 14, 4))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
customer._Automaton = _BuildAutomaton()




def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Norwegian_customer._UseForTag(pyxb.namespace.ExpandedName(None, 'firstname')), pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 22, 8))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Norwegian_customer._UseForTag(pyxb.namespace.ExpandedName(None, 'lastname')), pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 23, 8))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Norwegian_customer._UseForTag(pyxb.namespace.ExpandedName(None, 'country')), pyxb.utils.utility.Location('/home/crank/Projects/ext.gdml/srcs/_drafts/example.xsd', 24, 8))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Norwegian_customer._Automaton = _BuildAutomaton_()

