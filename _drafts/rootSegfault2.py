from __future__ import print_function

import ROOT
ROOT.TGeoMaterial.__init__._creates = False
ROOT.TGeoMedium.__init__._creates = False

def one( some ):
    outFile = ROOT.TFile('/tmp/one.root', 'recreate')
    #ROOT.gSystem.Load("libGeom")
    #ROOT.SetOwnership(ROOT.gGeoManager, False) 
    matVacuum = ROOT.TGeoMaterial("Vacuum", 0,0,0)
    matHelium = ROOT.TGeoMaterial("Helium", 0,0,0)
    medVacuum = ROOT.TGeoMedium("Vacuum",1, matVacuum)
    some['mat'] = matVacuum
    #vol = ROOT.gGeoManager.MakeBox("T", medVacuum, 270., 270., 120.)
    solid = ROOT.TGeoBBox( "TBox", 270., 270., 120 )
    vol = ROOT.TGeoVolume( "TVol", solid, medVacuum ) 
    ROOT.gGeoManager.SetTopVolume(vol)
    ROOT.gGeoManager.CloseGeometry()

def two():
    dct = {}
    if ROOT.gGeoManager:
        print( '#1 gGeoManager exists.' )
    else:
        print( '#1 gGeoManager does not exist.' )
    one( dct )
    if ROOT.gGeoManager:
        print( '#3 gGeoManager exists.' )
    else:
        print( '#3 gGeoManager does not exist.' )
