# -*- coding: utf8 -*-

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

"""
This script produces a self-consistent Geant4 GDML output
from template file based on pre-setted setup description
configuration.
"""

import os
from jinja2 import FileSystemLoader, Environment

def render_from_template(directory, templateName, **kwargs):
    """
    Function producing the template output of single template file.
    """
    loader = FileSystemLoader(directory)
    env = Environment(loader=loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template(templateName)
    return template.render(**kwargs)

def render_geometry_library( inDir, templateIndex, settingsDict, outDir ):
    """
    This routine constructs a copy of directory/file structure starting
    from directory path. Main routine producing GDML output.

    @param inDir input directory from where rootFile will be taken.
    @param rootFile a file inside inDir from which processing will start.
    @param settingsDict a settings dictionary.
    @param outDir output directory where processed gdml will be put.
    """
    dirs, files = get_file_hierarchy( inDir, templateIndex, settingsDict['detectors'], outDir )
    print( "Cloning directory structure:" )
    for dirName in dirs:
        dirAbsPath = os.path.join( outDir, dirName )
        if not os.path.isdir( dirAbsPath ):
            print( "  mkdir «%s»…"%dirAbsPath )
            os.makedirs( dirAbsPath )
        else:
            print( "  directory «%s» exists."%dirAbsPath )
    print( "Rendering templates:" )
    for tFile in files:
        print( "  generating «%s» -> «%s»…"%( tFile[0], tFile[1] ) )
        outputStr = render_from_template( inDir,
                                          os.path.relpath( tFile[0], inDir ),
                                          templatesDir=outDir,
                                          **settingsDict )
        with open( tFile[1], "w" ) as outFile:
            outFile.write( outputStr )
        print( "    …done (%d bytes in %d lines)."%( len(outputStr), outputStr.count('\n') ) )
    print( "All done." )

def get_file_hierarchy(inDir, templateIndex, detectorsCfg, outDir):
    """
    This routine constructs a copy of directory/file structure starting
    from directory path.
    @param inDir input directory from where rootFile will be taken.
    @param rootFile a file inside inDir from which processing will start.
    @param detectorsCfg a detectors dict.
    @param outDir output directory where processed gdml will be put.
    """
    #cPrfx = os.path.commonprefix( [templateIndex, inDir] )
    #if not cPrfx:
    #    raise RuntimeError("Couldn't get common prefix of index file and specified dir.")
    filesToProcess = [templateIndex]  # TODO: additional forced input files?
    for k, D in detectorsCfg.items():
        if 'geometryFile' in D.keys():
            if 'gdml' == D['geometryFile'][-4:]:
                # we need this file to be processed:
                filesToProcess.append( D['geometryFile'] )
            # elif: other file formats...
            else:
                raise RuntimeError( "Settings error: couldn't recognize file extension: ", D['geometryFile'] )
        # elif: other geometry specification ways...
        else:
            raise RuntimeError( "Detector «%s» has no geometry description!"%k )

    dirs = []
    inputFiles = []
    for f in filesToProcess:
        fullFilePathIn = ""
        fullFilePathOut = ""
        # produce input file path avoiding root ones:
        if '/' == f[0] and '.tgdml' == f[-6:]:
            fullFilePathIn = f
            fullFilePathOut = os.path.join( outDir, os.path.basename(f)[:-5] + 'gdml' )
        elif '.gdml' == D['geometryFile'][-5:]:
            fullFilePathIn  = os.path.join( inDir,  f[:-4] + 'tgdml' )
            fullFilePathOut = os.path.join( outDir, f[:-4] + 'gdml'  )
            thisOutDir = os.path.relpath( os.path.dirname( fullFilePathOut ), outDir )
            if thisOutDir not in dirs:
                dirs.append( thisOutDir )
        else:
            raise RuntimeError( "Don't know how to handle «%s» file."%f )
        if not os.path.isfile( fullFilePathIn ):
            raise RuntimeError( "File «%s» -> «%s» seems not to be a reachable file."%(f, fullFilePathIn) )
        inputFiles.append( (fullFilePathIn, fullFilePathOut) )
    return dirs, inputFiles



