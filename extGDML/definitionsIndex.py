#!/bin/env python
# -*- coding: utf8 -*-

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function

import os, sys
from dpath import dpath
from extGDML.clhepEvaluator import Evaluator as G4Evaluator
from types import MethodType
from pprint import pformat
import importlib

# Our routines expects certain form of definition dictionary, so it is
# better to specify generic topology here explicitly. However, the
# dpath.new() function will recursively create sub-dictionaries.
#definedObjects = {
#    'rootFile' : None,
#    'defines' : {
#        'quantities' : {},
#        'positions' : {},
#        'rotations' : {},
#        'matrices' : {}     # TODO
#    },
#    'materials' : {
#        'isotopes' : {},
#        'elements' : {},
#        'materials' : {}
#    },
#    'solids' : {}
#    # ...
#}

gGeant4SystemOfUnits = {
    "meter"     : 1.e+3,
    "kilogram"  : 1./1.60217733e-25,
    "second"    : 1.e+9,
    "ampere"    : 1./1.60217733e-10,
    "kelvin"    : 1.0,
    "mole"      : 1.0,
    "candela"   : 1.0
}

defaultTopology = {
     # defines section:
     'quantity' : 'defines/quantities/{name}',
     'position' : 'defines/positions/{name}',
     'rotation' : 'defines/rotations/{name}',
     'scale'    : 'defines/scales/{name}',
     'matrix'   : 'defines/matrices/{name}',
     # materials / media section:
     'isotope'  : 'materials/isotopes/{name}',
     'element'  : 'materials/elements/{name}',
     'material' : 'materials/materials/{name}',
     'medium'   : 'materials/media/{name}',  # note, that this path is used (*)
     # solids
     'solid'    : 'solids/{name}',
     # volumes:
     'volume'   : 'volumes/{name}'
}

def _generate_shortcuts( k ):
    # Getters:
    def _newgetter_closure(self, *args, **kwargs):
        return self.get( k, *args, **kwargs )
    _newgetter_closure.__name__ = 'get_%s'%k
    # Setters
    def _newsetter_closure(self, *args, **kwargs):
        self.set( k, *args, **kwargs )
    _newsetter_closure.__name__ = 'set_%s'%k
    return ( _newgetter_closure, _newsetter_closure )


def resolve_NIST_material( name, I, noexcept=False, **kwargs ):
    NIST_Idx = I.get_subindex( 'NISTMat' )
    if not NIST_Idx:
        sys.stderr('Have not NIST material index loaded whilst NIST material '
            'look-up requested.')
        return None
    return NIST_Idx.get_material( name, noexcept=noexcept, **kwargs )

class DefinitionsIndex(object):
    """
    Object aggregating definitions local to certain GDML file. Implemented as a
    set of wrappers and shortcuts over dictionary of user-defined topology.

    Constants and variables are represented as instances of Definition_t
    C-struct declared by CINT (so, one have to make CINT parse the
    common/structs.C).
    The scalar quantities are stored as instances of ComputedQuantity_t
    struct.
    Rotations, positions or scales 3-dimensional vector quantity are stored
    as instances of Vector3_t struct.

    The CLHEP evaluator instance is a member of this class.
    
    It is implied that
    for each parsed GDML file one will instantiate new DefinitionsIndex
    instance. It is convinient to store the hierarchically according to GDML
    files with modular topology.
    """
    supportedEntities = (   # Defines section:
                            'quantity', 'position', 'rotation', 'matrix',
                            # Materials section:
                            'isotope',  'element',  'material', 'medium',
                            # Solids section:
                            'solid',
                            # Structure section:
                            'volume'
                        ) 
    def __init__(self,
                 units=gGeant4SystemOfUnits,
                 topology=defaultTopology,
                 suppMaterialsResolver=None,
                 parentIndex=None
                 ):
        self.parentIndex = parentIndex
        self._topology = defaultTopology
        self.units = units
        self.E = G4Evaluator()
        su = [
            units["meter"],     units["kilogram"],      units["second"],
            units["ampere"],    units["kelvin"],        units["mole"],
            units["candela"]
        ]
        self.E.setSystemOfUnits( *su )
        self.E.setStdMath()
        # definitions dictionary:
        self.D = {}
        # create shortcut methods:
        for k in self.supportedEntities:
            _newgetter_closure, _newsetter_closure = _generate_shortcuts(k)
            setattr( self.__class__,
                _newgetter_closure.__name__,
                _newgetter_closure )
            setattr( self.__class__,
                _newsetter_closure.__name__,
                _newsetter_closure )
        if( suppMaterialsResolver ):
            def _supp_material_getter( self, name, noexcept=False, **kwargs ):
                matInst = self.get('material', name, noexcept=True,
                                    **kwargs)
                if not matInst:
                    importedMat = suppMaterialsResolver(name, self,
                                            noexcept=noexcept, **kwargs)
                    if importedMat:
                        self.set_material( importedMat, name=name,
                                           noexcept=noexcept )
                    else:
                        # causes ordinary exception
                        return self.get('material', name, noexcept=noexcept,
                                        **kwargs)
                    return importedMat
                else:
                    return matInst
            self.get_material = MethodType(_supp_material_getter, self)
        self.subidxs = {}

    def get(self, entityTypeName, name, noexcept=False, level=0, **kwargs):
        if not entityTypeName in self.supportedEntities:
            raise SystemError( 'Got unsupported entity type identifier '
                               '"%s" for getter.'%entityTypeName )
        path = self._topology[entityTypeName].format(name=name, **kwargs)
        try:
            return dpath.get( self.D, path )
        except KeyError:
            # xxx: apparently no need in that sophistication --- Geant4 seems
            # to put all the stuff into one dictionary.
            #if self.parentIndex:
            #    return self.parentIndex.get( entityTypeName, name,
            #            noexcept=noexcept, level=(level+1), **kwargs )
            if noexcept:
                return None
            else:
                print( 'KeyError raised for index having definitions:\n', self )
                print( 'while looking up for %s instance "%s".'%(entityTypeName,
                        name ) )
                raise

    def set(self, entityTypeName, instance, override=False, **kwargs):
        if not entityTypeName in self.supportedEntities:
            raise SystemError( 'Got unsupported entity type identifier '
                               '"%s" for setter.'%entityTypeName )
        path = self._topology[entityTypeName].format(**kwargs)
        exists = True
        try:
            dpath.get( self.D, path )
        except KeyError:
            exists = False
        if exists:
            sys.stderr.write( '%s already contains an entity. '
                              'Overriden (%s).\n'%(path, ('ok' if override else 'warn')) )
        if override:
            dpath.set( self.D, path, instance )
        else:
            dpath.new( self.D, path, instance )

    def __str__(self):
        return pformat( self.D )

    def get_media_count(self):
        # Hardcoded to topology (*)
        if 'materials' in self.D.keys():
            if 'media' in self.D['materials'].keys():
                return 1 + len( dpath.util.get( self.D, 'materials/media') )
        return 1

    def new_subindex(self, name, *args, **kwargs):
        si = DefinitionsIndex(*args, parentIndex=self,
                              units=self.units, **kwargs)
        self.subidxs[name] = si
        return si

    def get_subindex( self, name, noexcept=False ):
        if not noexcept:
            return self.subidxs[name]
        else:
            return self.get(name, None)

