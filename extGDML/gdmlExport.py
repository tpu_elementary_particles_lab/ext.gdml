#!/bin/env python
# -*- coding: utf8 -*-

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function

import importlib

def export( gdml, index, exportFormat='GDML', *args, **kwargs ):
    entitiesList = [
            'definitions',  'materials',
            'solids',       'structure',
            'setup' ]
    sectionsExporters = []
    for entitiesName in entitiesList:
        if not kwargs.get( 'read' + entitiesName.title(), True ):
            sectionsExporters.append( None )
        else:
            m = importlib.import_module( 'extGDML.' \
                                        + exportFormat.lower() \
                                        + '._0%d'%(1+len(sectionsExporters)) \
                                        + '_' + entitiesName )
            f = getattr( m, 'read_' + entitiesName )
            sectionsExporters.append( f )
    for export in sectionsExporters:
        if export:
            export( gdml, index, *args, **kwargs )

