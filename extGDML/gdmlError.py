# -*- coding: utf8 -*-

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function
import sys

"""
This module describes an exception structure.
"""

class GDMLError(Exception):
    """
    Common class for unspecific extGDML error.
    """
    def __init__( self, details=None ):
        self.details = details

    def __str__(self):
        return repr( self.details )

def aux_evaluator_statuscode_to_str( code ):
    from extGDML import Evaluator
    if code == Evaluator.OK:
        return "ok"
    elif Evaluator.WARNING_EXISTING_VARIABLE == code:
        return "(warn) existing variable"
    elif Evaluator.WARNING_EXISTING_FUNCTION == code:
        return "(warn) existing function"
    elif Evaluator.WARNING_BLANK_STRING == code:
        return "(warn) blank string"
    elif Evaluator.ERROR_NOT_A_NAME == code:
        return "(err) not a name"
    elif Evaluator.ERROR_SYNTAX_ERROR == code:
        return "(err) syntax error"
    elif Evaluator.ERROR_UNPAIRED_PARENTHESIS == code:
        return "(err) unpaired parenthesis"
    elif Evaluator.ERROR_UNEXPECTED_SYMBOL == code:
        return "(err) unexpected symbol"
    elif Evaluator.ERROR_UNKNOWN_VARIABLE == code:
        return "(err) unknown variable"
    elif Evaluator.ERROR_UNKNOWN_FUNCTION == code:
        return "(err) unknown function"
    elif Evaluator.ERROR_EMPTY_PARAMETER == code:
        return "(err) empty parameter"
    elif Evaluator.ERROR_CALCULATION_ERROR == code:
        return "(err) calculation error"
    else:
        return "(?) unknown status code"

def evaltr_status_is_warning( statusCode ):
    from extGDML import Evaluator
    return statusCode == Evaluator.WARNING_EXISTING_VARIABLE \
        or statusCode == Evaluator.WARNING_EXISTING_FUNCTION\
        or statusCode == Evaluator.WARNING_BLANK_STRING

def evaluator_warning( E, expectedStatus=None, **kwargs ):
    txtErr = E.error_name()
    if not txtErr.strip():
        txtErr = aux_evaluator_statuscode_to_str(E.status())
    sys.stderr.write( 'Evaluator WARNING({code}{expected}): at expression "{expr}", position ' \
           '{position} {supp} --- {txtErr}\n'.format(**{
                   'code' : E.status(),
               'position' : E.error_position(),
                 'txtErr' : txtErr,
                   'expr' : kwargs.get('expr', '<unknown>'),
                   'supp' : kwargs.get('details', ''),
               'expected' : '' if not expectedStatus else ' (expected %d)'%expectedStatus
            }) )

class GDMLEvaluatorError(Exception):
    """
    An exception thrown if CLHEP evaluator issues an error.
    """
    def __init__(self, gdmlEvltr, *args, **kwargs ):
        #gdmlEvltr.print_error()
        txtErr = gdmlEvltr.error_name()
        if not txtErr.strip():
            txtErr = aux_evaluator_statuscode_to_str(gdmlEvltr.status())
        self.details = {
                'code' : gdmlEvltr.status(),
            'position' : gdmlEvltr.error_position(),
              'txtErr' : txtErr,
                'expr' : kwargs.get('expr', '<unknown>'),
                'supp' : kwargs.get('details', '')
        }
        print( self )

    def __str__(self):
        return repr('at expression:\"' \
                    '{expr}\" {supp} ' \
                    'GDML parser issued an error {code} at position {position} --- "{txtErr}"'.format(**self.details) )

