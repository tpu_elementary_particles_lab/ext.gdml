# -*- coding: utf8 -*-
# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function

"""
Defines some convinient routines to provide automatic arguments mapping
from class field to arguments of third-partyroutines.
"""

import sys, re
from extGDML.gdmlError import GDMLError
from string import Formatter

argParseFmt = re.compile(
r'^(?:(@)?(?P<name>[\w/]+)|(?P<isCst>!))'
  '(?:\:(?:(?P<quantity>(length|angle))\[(?P<unit>[-+/*\w]+)\]))?'
  '(?P<opt>\?)?'
  '(?:=(?P<formula>.+))?$' )

def parse_argument_mapping_str( argStr ):
    #print( ">>> ", argStr )
    m = re.match( argParseFmt, argStr )
    if not m:
        raise ValueError( 'Mapping description "%s" does not match to '
                          'regex.'%argStr )
    return {
                'isAttr' : True if m.group(1) else False,
               'isConst' : True if m.group('isCst') else False,
                  'name' : m.group('name'),
              'quantity' : m.group('quantity'),
            'isOptional' : True if m.group('opt') else False,
                  'unit' : m.group('unit'),
               'formula' : m.group('formula'),
              'original' : argStr
        }

def push_argument( E, destArgsLst, argDct, nodeInstance, quiet=False ):
    a = None
    if argDct['isAttr']:
        a = getattr(nodeInstance, 'get_' + argDct['name'] )()
    elif argDct['isConst']:
        pass
    else:
        # TODO: support for elements and XPath-like resolution
        raise NotImplementedError(
                "Resolution of elements arent't yet supported." )
    if not a is None:
        if argDct.get('quantity', None):  # TODO: elaborated support?
            # Reusable snippet?
            unit = None
            if 'length' == argDct['quantity']:
                unit = nodeInstance.get_lunit()
            elif 'angle' == argDct['quantity']:
                unit = nodeInstance.get_aunit()
            else:
                raise ValueError( 'Could not determine units of quantity "%s".',
                    argDct['quantity'] )
            a = E.convert_units( a, fromUnit=unit, toUnit=argDct['unit'] )
        if argDct['formula']:
            formulaParametersNames = [ nm[1] for nm in Formatter().parse(argDct['formula']) ]
            formulaParametersNames = filter( lambda t : (not not t) and t != '_', formulaParametersNames )
            formulaParameters = {
                nm : getattr( nodeInstance, 'get_' + nm )()
                for nm in formulaParametersNames }
            formulaParameters.update({'_' : a})
            s = argDct['formula'].format(**formulaParameters)
            if not quiet:
                print('    ...=',s)
            a = E.evaluate( s )
        destArgsLst.append( a )
    elif argDct['isConst']:
        s = '(%s)'%argDct['formula']
        u1, u2 = 1, 1
        if argDct.get('quantity', None):
            if 'length' == argDct['quantity']:
                u1 = nodeInstance.get_lunit()
            elif 'angle' == argDct['quantity']:
                u1 = nodeInstance.get_aunit()
            else:
                raise ValueError( 'Unable to get units for quantity "%s"'%
                        argDct['quantity'] )
            u2 = argDct['unit']
            s += '*(({u1})/({u2}))'.format(u1=u1, u2=u2)
        if not quiet:
            print( '!', s )
        a = E.evaluate(s)
        destArgsLst.append( a )
    elif not argDct['isOptional']:
        raise GDMLError( "Element didn't provide %s %s while it is required."%(
            'attribute' if argDct['isAttr'] else 'element',
            argDct['name']) )
    else:
        pass  # it's optional --- ok

#if __name__ == "__main__":
#    print( parse_argument_mapping_str( "@some:length" ) )
#    print( parse_argument_mapping_str( "other/element" ) )
#    print( parse_argument_mapping_str( "@another/attribute?={value}/2" ) )
#exit(0)

def dynamic_import(name):
    components = name.split('.')
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod

def auto_CSG_ctr( constructor_,
                  *args ) :
    """
    Function returns a closure that performs construction of particular
    CSG primitive of third-party code.

    Argument description syntax:
        (!)|([@][<name>)][:quantity\[unit\]][?][=formula({v})]
         ^    ^   ^^^^    ^^^^^^^^   ^^^^   ^  ^
         │    │   │       │          │      │  └─ simple arithmetic to be evaluated
         │    │   │       │          │      └─ indicates, that arg can be omitted
         │    │   │       │          └─  units required by dest. function
         │    │   │       └─ treat as a quantity (with units)
         │    │   └─ look-up name for sub-element/attribute
         │    └─ it is an attribute (use `get_<name>')
         └─ indicates that the parameter is set to expression.

    @parameter constructor a name of CSG primitive (can be
               function that have to accept a parsed GDML object and return
               a string referring to a particular ctr name).
    @parameter argMapping a tuple reflict arguments order
    """
    ctrArgsDict = None
    if type(args[0]) is dict:
        ctrArgsDict = {}
        for k, v in args[0].iteritems():
            ctrArgsDict[k] = [ parse_argument_mapping_str(a) for a in v ]
    else:
        ctrArgsDict = [ parse_argument_mapping_str(a) for a in args ]
    def _res_closurefunc( E, nodeInstance, quiet=False, defines={} ):
        args_ = []
        constructor = constructor_
        ad = ctrArgsDict
        # Resolve a ctr:
        if callable( constructor ):
            constructor = constructor( nodeInstance, defines=defines )
            ad = ctrArgsDict[constructor]
        ctrStr = constructor
        for argDct in ad:
            try:
                push_argument( E, args_, argDct, nodeInstance, quiet=quiet )
            except Exception as e:
                sys.stderr.write( 'While evaluating arglist %s\n'%argDct.__str__() )
                raise
        if not quiet:
            print( '(dev) arguments prepared for \"%s\":'%ctrStr, *args_ )
        return dynamic_import(constructor)(*args_)
    return _res_closurefunc

