# -*- coding: utf8 -*-

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function
from ._01_definitions import vector_quantity_to_root_struct, \
                                 rotation_from_v3, \
                                 position_from_v3, \
                                 scale_from_v3

sentinel = object()

def _get_or_resolve_closures_generator( attrName,
                                        inline_translator,
                                        overrideGetterTag=None ):
    """
    Generates a function getting or sesolving certain ROOT object.
    Implies that element `gdmlNode' contains either element named
    `attrName', either element named `attrName' + 'ref'. E.g.:
    '<position/>' or '<positionref/>', '<rotation/>' or '<rotationref/>',
    etc.
    If reference is given he generated function will perform look-up in
    the index object (parameter `I').
    If lement is embedded (inline) the translator function will be invoked to
    perform construction of specific anonymous ROOT object.
    """
    def _get_or_resolve_closure( gdmlNode, I, default=sentinel ):
        # First, try to acquire inline-defined object:
        gdmlObj = getattr( gdmlNode, 'get_' + attrName )()
        if gdmlObj is None:
            # Acquizition of inline failed, try to resolve reference:
            elRef = getattr( gdmlNode, 'get_' + attrName + 'ref' )()
            if elRef is None:
                if default is sentinel:
                    raise KeyError( 'Neither element "%s", nor reference to it '
                        'were found in element "%s"'%(attrName, str(gdmlNode)) )
                else:
                    return default
            return I.get( overrideGetterTag if overrideGetterTag else attrName,
                          elRef.get_ref() )
        else:
            # Inline acquizition succeeded, invoke translator:
            return inline_translator( gdmlObj, I.E, qTag=attrName )
    return _get_or_resolve_closure

def _v3_root_struct_constructor( gdmlObject, E, qTag=None ):
    import ROOT
    destStr = ROOT.Vector3_t()
    vector_quantity_to_root_struct( gdmlObject, E, destStr )
    rootStruct=None
    if 'position' == qTag:
        rootStruct = position_from_v3( destStr )
    elif 'rotation' == qTag:
        rootStruct = rotation_from_v3( destStr )
        print( 'XXX: rotation ctr invoked:', destStr, '->', rootStruct )  # XXX
    elif 'scale' == qTag:
        rootStruct = scale_from_v3( destStr )
    else:
        raise ValueError("Automatic conversion for tag \"%s\" unimplemented." \
            %str(destStr.qType) )
    return (destStr, rootStruct)

get_or_resolve_position = \
    _get_or_resolve_closures_generator( 'position',
                                        _v3_root_struct_constructor )
get_or_resolve_rotation = \
    _get_or_resolve_closures_generator( 'rotation',
                                        _v3_root_struct_constructor )

get_or_resolve_direction = \
    _get_or_resolve_closures_generator( 'direction',
                                        _v3_root_struct_constructor,
                                        overrideGetterTag='position' )

get_or_resolve_direction = \
    _get_or_resolve_closures_generator( 'scale',
                                        _v3_root_struct_constructor )

