#!/bin/env python
# -*- coding: utf8 -*-

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function

"""
File describes routines for conversion the GDML structure hierarchy definition
into corresponding ROOT TGeo instances.

Shouldn't be used as a separate module. Import extGDML.root.bindings instead.
"""

import ROOT

def dbg_dump_nodes( el, level=1 ):
    print( ' '*4*level, '- GetName() ->', el.GetName() )
    if( not el.GetShape() ):
        raise RuntimeError('Shapeless volume!')
    print( ' '*4*(level), '  ... GetShape() -> ', el.GetShape() )
    print( ' '*4*(level), '  ... IsAssembly() -> ', el.GetShape().IsAssembly() )
    if el.GetNodes():
        print( ' '*4*(level), '  ...', el.GetNodes().GetSize(), 'child nodes' )
        for i in range( 0, el.GetNodes().GetSize() ):
            n = el.GetNode(i)
            if not n:
                print( ' '*4*(level), '  ... #', i, 'NULL node' )
                continue
            if n.GetVolume():
                subVol = n.GetVolume()
            else:
                print( ' '*4*(level), '  ... #', i, '<no volume>' )
            if n.GetNodes():
                dbg_dump_nodes( subVol, level=(level+1) )
    else:
        print( ' '*4*level, '    - No child nodes.' )


def read_setup( gdml, I,
                quiet=False, setupName='Default',
                auxResults={}, *args, **kwargs ):
    setupDescription = None
    namedSetups = filter( lambda el: setupName == el.get_name(),
                          gdml.get_setup() )
    if len( namedSetups ) > 1:
        raise NotImplementedError( 'Selection by setup name "%s" leads to '
                'disambiguation; versions indexing are not supported yet.'% \
                setupName )
    elif not namedSetups:
        raise KeyError( "Has not setup named \"%s\"."%setupName )
    else:
        setupDescription = namedSetups[0]
    auxResults['topVolume'] = I.get_volume( setupDescription.get_world().get_ref() )

    if kwargs.get('finalizeGeometry', False):
        if not quiet:
            print( 'Closing geometry...' )
        # XXX: dev
        if False:
            for k, obj in I.D['volumes'].iteritems():
                #if 'World' == k: continue  # since will always trigger
                print( 'Checking volume %s:'%k )
                dbg_dump_nodes(obj)
                #print( '    - IsAssembly() ->', obj.IsAssembly() )
                #obj.PrintNodes()
        ROOT.gGeoManager.SetTopVolume( auxResults['topVolume'] )
        ROOT.gGeoManager.CloseGeometry()
        if ROOT.gFile:
            ROOT.gGeoManager.Write()
    return auxResults['topVolume']

