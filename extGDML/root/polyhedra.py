# -*- coding: utf8 -*-
# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function
import ROOT


def pgon_ctr( E, nodeInstance, quiet=False, defines={},
                toLUnit='cm', toAUnit='degree'):
    """
    Polyhedra (polygon) CSG ctr. For non-generic case it can be imagined as
    a sequence of prisms with scalable transversal cross section planes. In
    GDML they're defined as element with variable (>=1) number of cross
    sections.
    """
    givenLUnit = nodeInstance.get_lunit()
    givenAUnit = nodeInstance.get_aunit()
    ctrArgs = [
            nodeInstance.get_name(),  # name
            E.convert_units( nodeInstance.get_startphi(),
                             fromUnit=givenAUnit,
                             toUnit=toAUnit ), # phi,
            E.convert_units( nodeInstance.get_deltaphi(),
                             fromUnit=givenAUnit,
                             toUnit=toAUnit ), # dphi,
            E.eval_int( nodeInstance.get_numsides() ), # nedges,
            len( nodeInstance.get_zplane() ) # nz
        ]
    if not quiet:
        print( 'Polyhedra args: ', ctrArgs )
    pgon = ROOT.TGeoPgon( *ctrArgs )
    nzPlane = 0
    for zPlaneNode in nodeInstance.get_zplane():
        sectArgs = [
                nzPlane, # snum
                E.convert_units( zPlaneNode.get_z(),
                             fromUnit=givenLUnit,
                             toUnit=toLUnit ), # z
                E.convert_units( zPlaneNode.get_rmin(),
                             fromUnit=givenLUnit,
                             toUnit=toLUnit ), # rmin
                E.convert_units( zPlaneNode.get_rmax(),
                             fromUnit=givenLUnit,
                             toUnit=toLUnit ), # rmax
            ]
        if not quiet:
            print( '    + Z-Plane: ', sectArgs )
        pgon.DefineSection( *sectArgs )
        nzPlane += 1
    return pgon


