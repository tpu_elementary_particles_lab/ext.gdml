#!/bin/env python
# -*- coding: utf8 -*-

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function

"""
File describes routines for conversion the GDML solids definitions into
corresponding ROOT structures.

Shouldn't be used as a separate module. Import extGDML.root.bindings instead.
"""

from extGDML.root.CSG import gCSGConstructorsDict
from sys import stderr
import ROOT

def read_solids( gdml, I,
                 quiet=False, *args, **kwargs ):
    """
    This function is designed to work with <solids/> section of GDML document.
    Parsed entities here steer the creation of ROOT TGeo entities (CSG
    primitives, boolean operations and replicas). Created instances are then
    indexed by name in `defs' dictionary at 'solids/'.
    """
    from extGDML.root.loop import treat_loop_element
    for solid in gdml.get_solids().Solid:
        className = solid.__class__.__name__
        ctr = gCSGConstructorsDict.get( className, None )
        if ctr is None:
            stderr.write( 'WARNING: CSG solid class "%s" is not yet '
                          'supported. Omitting construction --- errors '
                          'are possible.\n'%className )
            continue
        solidTObj = ctr( I.E, solid, quiet=quiet, defines=I )
        I.set_solid( solidTObj, name=solid.get_name() )

