#!/bin/env python
# -*- coding: utf8 -*-

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function

from extGDML.gdmlParse import parse_gdml
from extGDML.definitionsIndex import DefinitionsIndex
from extGDML.gdmlExport import export as export_gdml

"""
File containing entry point for importing GDML file.
"""

def export_gdml_file(
            inputFileName,
            verboseParse=False,
            verboseConvert=False,
            index=None, indexClass=DefinitionsIndex,
            # Auxilliary GDML file containing NIST materials dump:
            NISTMaterialsFilePath=None,
            # Have sense only if index=None and indexClass!=None
            indexCtrArgs=[], indexCtrKWArgs={},
            *args,
            **kwargs
        ):
    # Parse GDML file:
    gdml, isValid = parse_gdml( inputFileName, silence=(not verboseParse) )
    if not isValid:
        raise RuntimeError('GDML file "%s" is not valid.'%inputFileName )
    # Initialize index:
    if not index:
        if not indexClass:
            raise TypeError('Neither index, neither index class provided.')
        index = indexClass( *indexCtrArgs, **indexCtrKWArgs )
    else:
        indexClass = index.__class__

    # Parse supplementary NIST materials file, if need:
    NISTMaterialsGDML = kwargs.pop('NISTMaterialsGDML', None)
    if NISTMaterialsFilePath:
        subIdxNISTmats = index.new_subindex( 'NISTMat' )
        NISTMaterialsGDML, isValid = parse_gdml( NISTMaterialsFilePath,
                                            silence=(not verboseParse) )
        if NISTMaterialsGDML and isValid:
            if verboseParse:
                print( "NIST materials parsed." )
        else:
            sys.stderr.write( "Trouble occured while reading out NIST "
                              "materials.\n" )
    # Convert:
    export_gdml( gdml, index,
                 *args,
                 NISTMaterialsGDML=NISTMaterialsGDML,
                 quiet=(not verboseConvert),
                 verboseParse=verboseParse,
                 verboseConvert=verboseConvert,
                 **kwargs )
    return index


