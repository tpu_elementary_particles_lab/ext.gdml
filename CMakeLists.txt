# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# Author: Bogdan Vasilishin <togetherwithra@gmail.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

cmake_minimum_required( VERSION 3.5 )
project( extGDML LANGUAGES )

set( extGDML_VERSION_MAJOR 0  )
set( extGDML_VERSION_MINOR 1  )
set( extGDML_VERSION_PATCH dev )
set( extGDML_VERSION ${extGDML_VERSION_MAJOR}.${extGDML_VERSION_MINOR}.${extGDML_VERSION_PATCH} )

# This CMake file performs general pre-setup routines for extGDML package
# including building the C++ extension module, generating python bindings,
# and parser for GDML. Its production is intended to fit the python's
# setuptools framework, however does not perform full intallation procedure
# upon the `$ make install' call invoked from building directory. It will,
# instead, install only extGDMLc++.so library while the python package needs
# to be installed with python's conventional procedures (setuptools).

option( CXX_INTEGRATION
    "Whether to build a C++ extension library." ON )
option( PYTHON_INTEGRATION
    "Whether to build python parser and processor." ON )
option( GENERATE_DEVINSTALL_SYMLINKS
    "Creates additional symlinks to support development pip-installation." OFF )

if( PYTHON_INTEGRATION )
    # Fetch and patch schema for automatic generation of parser.
    add_subdirectory( schema )
    # ?
    #enable_testing()
    # If PYTHON_EXECUTABLE is overriden, use it for virtualenv. Instead, set
    # it to just python2.
    if( PYTHON_EXECUTABLE )
        set( VENV_PYTHON2_EXEC ${PYTHON_EXECUTABLE} )
    else( PYTHON_EXECUTABLE )
        set( VENV_PYTHON2_EXEC python2 )
    endif( PYTHON_EXECUTABLE )
    # Find Python and Virtualenv. We don't actually use the output of the
    # find_package, but it'll give nicer errors.
    find_package(PythonInterp 2 REQUIRED)
    find_program(VIRTUALENV virtualenv REQUIRED)
    if(NOT VIRTUALENV)
        message(FATAL_ERROR "Could not find `virtualenv` in PATH")
    else()
        # "interim" here means that it will be used for generating temporary
        # sources.
        message(STATUS "Found (interim) virtualenv exec: ${VIRTUALENV}")
    endif()
    # Generate the interim virtualenv and ensure it's up to date.
    add_custom_command(
       OUTPUT .venv
       COMMAND ${VIRTUALENV} -p${VENV_PYTHON2_EXEC} --no-site-packages --prompt='(venv-2) ' .venv
    )
    add_custom_command(
        OUTPUT venv.stamp
        DEPENDS .venv requirements.txt
        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/requirements.txt requirements.txt
        COMMAND ./.venv/bin/pip install pip --upgrade
        COMMAND ./.venv/bin/pip install -r requirements.txt
    )
    # Automatically generate a GDML parser based on XSD schema
    set( EXTGDML_DIR ${CMAKE_CURRENT_SOURCE_DIR}/extGDML )
    file( GLOB SCHEMA_XSD_FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/schema/gdml/*.xsd )
    file( COPY ${CMAKE_CURRENT_SOURCE_DIR}/gdml_methods.py
          DESTINATION ${CMAKE_CURRENT_BINARY_DIR} )
    add_custom_command(
        OUTPUT ${EXTGDML_DIR}/gdmlClasses.py
               ${EXTGDML_DIR}/gdmlSubclasses.py
        DEPENDS venv.stamp ${SCHEMA_XSD_FILES}
        COMMAND source ./.venv/bin/activate \;
                ./.venv/bin/generateDS.py -m -f
                            -o ${EXTGDML_DIR}/gdmlClasses.py
                            --export='write literal etree' --root-element gdml
                -s ${EXTGDML_DIR}/gdmlSubclasses.py
                -u gdml_methods
                ${CMAKE_CURRENT_SOURCE_DIR}/schema/gdml/gdml.xsd
        COMMENT "Generating the python GDML parser..." )

    add_custom_target( extGDML-All ALL
        DEPENDS ${EXTGDML_DIR}/gdmlClasses.py
                ${EXTGDML_DIR}/gdmlSubclasses.py
                ${INTEGRATION_LIB} )

    # Build extGDML library and CLHEP evaluator extension
    # We need our CLHEP evaluator extension to be available from virtualenv as
    # well as custom future python bindings to C++ classes, so the subsequent
    # CMake call has to be invoked from inside of virtual environment. This kind
    # of a bit tricky, however being the less evil in dovetailing two build
    # systems (CMake and Python's setuptools).
    # The lines below calls another CMake build from within current.
    add_subdirectory( clhepEvaluator )
    configure_file( ${PROJECT_SOURCE_DIR}/setup.py.in
                    ${PROJECT_BINARY_DIR}/setup.py )
    #
    # Note: symlinks below normally have to be created only for customized
    # installation path since pip in development mode does not do his job well
    # while gathering information about packages outside root package
    # directory.
    if( GENERATE_DEVINSTALL_SYMLINKS )
        add_custom_command(
            TARGET extGDML-All POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E create_symlink
                ${PROJECT_SOURCE_DIR}/extGDML
                ${PROJECT_BINARY_DIR}/extGDML )
        add_custom_command(
            TARGET extGDML-All POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E create_symlink
                ${PROJECT_SOURCE_DIR}/tests
                ${PROJECT_BINARY_DIR}/tests )
        add_custom_command(
            TARGET extGDML-All POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E create_symlink
                ${PROJECT_SOURCE_DIR}/sVbp
                ${PROJECT_BINARY_DIR}/sVbp )
    endif( GENERATE_DEVINSTALL_SYMLINKS )
endif( PYTHON_INTEGRATION )

if( CXX_INTEGRATION )
    add_subdirectory( cxx_integration )
endif( CXX_INTEGRATION )

