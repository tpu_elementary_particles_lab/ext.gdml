# extGDML examples dir

This directory contains few examples demonstrating capabilities of package.

 * `csg-simple.gdml` describes a simple CSG — a lead cube with subtracted orb
immersed into air world box. Originally written to test GDML-2-TGeo conversion.
 * `replica-simple.gdml` describes a linearilly replicated orbs inside of a box.
 * `replica-radial.gdml` describes a radially replicated cylinders inside of a
another cyl.
 * `multifile/` describes a detector library. For convinience, GDML data
related to particular experiment can be arranged in a form of geomtrical
library (see README file inside dir for further details).

