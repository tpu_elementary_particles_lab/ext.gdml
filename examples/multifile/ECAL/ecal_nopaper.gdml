<?xml version="1.0" encoding="UTF-8" ?>

<!--
 Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 Author: Renat R. Dusaev <crank@qcrypt.org>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-->

<!DOCTYPE gdml [
    <!ENTITY defines    SYSTEM "../01_defines.igdml">
    <!ENTITY materials  SYSTEM "../02_materials.igdml">
    <!ENTITY solids     SYSTEM "../03_solids.igdml">
]>

<gdml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation="http://service-spi.web.cern.ch/service-spi/app/releases/GDML/schema/gdml.xsd">

    <define>
        &defines;

        <!-- paper1, cnv, paper2, scn -->
        <position name="cnvOffset"          x="0" y="0" z="-(ScnPlDepth_mm)/2" unit="mm"/>
        <position name="scnOffset"          x="0" y="0" z=" (CnvPlDepth_mm)/2" unit="mm"/>
        <position name="preshowerOffset"    x="0" y="0" z="-ECALDepth_noP_mm/2" unit="mm"/>
        <position name="ecalOffset"         x="0" y="0" z="PreshowerDepth_noP_mm/2" unit="mm"/>
        
    </define>

    <materials>
        &materials;
    </materials>

    <solids>
        &solids;

        <box name="geoECALEntire" x="SizeX"
                                  y="SizeY"
                                  z="OverallECALDepth_noP"/>

        <box name="geoECAL" x="SizeX"
                            y="SizeY"
                            z="ECALDepth_noP"/>

        <box name="geoPreshower" x="SizeX"
                                 y="SizeY"
                                 z="PreshowerDepth_noP"/>

        <box name="geoCell" x="SizeX"
                            y="SizeY"
                            z="CellDepth_noP"/>

        <!-- TODO: for segmentated version (do we need it?)
        <box name="geoScnPl" x="CellSide/2"
                             y="CellSide/2"
                             z="ScnPlDepth/2"/>

        <box name="geoCnvPl" x="CellSide/2"
                             y="CellSide/2"
                             z="CnvPlDepth/2"/>
         -->
        <box name="geoScnPl" x="SizeX"
                             y="SizeY"
                             z="ScnPlDepth"/>

        <box name="geoCnvPl" x="SizeX"
                             y="SizeY"
                             z="CnvPlDepth"/>		
		<!--Same as geoCnvPl					 
        <box name="preshowerCnvPl"  x="SizeX"
                                    y="SizeY"
                                    z="CnvPlDepth"/>
        -->
    </solids>

    <structure>
        <volume name="cnvLayer">
            <materialref ref="mtLead"/>
            <solidref ref="geoCnvPl"/>
            <auxiliary auxtype="style" auxvalue="*:!hidden/"/>
            <auxiliary auxtype="style" auxvalue="StandaloneCell:#333333ff,!surface"/>
        </volume>
        
        <volume name="scnLayer">
            <materialref ref="G4_PLASTIC_SC_VINYLTOLUENE"/>
            <solidref ref="geoScnPl"/>
            <auxiliary auxtype="style" auxvalue="*:!hidden/"/>
            <auxiliary auxtype="style" auxvalue="StandaloneCell:#33663377,!surface"/>
            <auxiliary auxtype="SensDet" auxvalue="MKTriggerSD"/>
        </volume>

        <volume name="Cell">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoCell"/>
            <physvol>
                <volumeref ref="cnvLayer"/>
                <positionref ref="cnvOffset"/>
            </physvol>
            <physvol>
                <volumeref ref="scnLayer"/>
                <positionref ref="scnOffset"/>
            </physvol>
            <auxiliary auxtype="style" auxvalue="*:!hidden"/>
            <auxiliary auxtype="style" auxvalue="Detailed:!wireframe"/>
        </volume>

        <volume name="preshower">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoPreshower"/>
            <replicavol number="16"> <!-- LongitudalPreshowerN -->
                <volumeref ref="Cell"/>
                <!-- <positionref ref="preshowerCnvOffset"/> -->
                <replicate_along_axis>
                    <direction z="1"/>
                    <width  value="ScnPlDepth_mm + CnvPlDepth_mm"   unit="mm"/>
                    <offset value="0"              unit="mm"/>
                </replicate_along_axis>
            </replicavol>
            <auxiliary auxtype="style" auxvalue="*:!wireframe"/>
        </volume>

        <volume name="ECAL">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoECAL"/>
            <replicavol number="150"> <!-- LongitudalN -->
                <volumeref ref="Cell"/>
                <!-- <positionref ref="preshowerCnvOffset"/> -->
                <replicate_along_axis>
                    <direction z="1"/>
                    <width  value="ScnPlDepth_mm + CnvPlDepth_mm"   unit="mm"/>
                    <offset value="0"              unit="mm"/>
                </replicate_along_axis>
            </replicavol>
            <auxiliary auxtype="style" auxvalue="*:!wireframe"/>
        </volume>

        <volume name="entireECAL">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoECALEntire"/>
            <physvol>
                <volumeref ref="preshower"/>
                <positionref ref="preshowerOffset"/>
            </physvol>
            <physvol>
                <volumeref ref="ECAL"/>
                <positionref ref="ecalOffset"/>
            </physvol>
            <auxiliary auxtype="style" auxvalue="*:!wireframe"/>
        </volume>
        
    </structure>

    <!-- This one is used when embedding ECAL as a module -->
    <setup name="Default" version="1.0" >
        <world ref="entireECAL" />
    </setup>
    <!-- Single cell viewing -->
    <setup name="StandaloneCell" version="1.0" >
        <world ref="Cell" />
    </setup>
    <setup name="Detailed" version="1.0" >
        <world ref="entireECAL" />
    </setup>
    <!-- Use this one for standalone view of ECAL
    <setup name="ECALDefault" version="1.0" >
        <world ref="ECAL_World" />
    </setup> -->
</gdml>

<!--
vi:ft=xml
-->
