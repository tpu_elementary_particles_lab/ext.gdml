<?xml version="1.0" encoding="UTF-8" ?>
{# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 # Author: Renat R. Dusaev <crank@qcrypt.org>
 # 
 # Permission is hereby granted, free of charge, to any person obtaining a copy of
 # this software and associated documentation files (the "Software"), to deal in
 # the Software without restriction, including without limitation the rights to
 # use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 # the Software, and to permit persons to whom the Software is furnished to do so,
 # subject to the following conditions:
 # 
 # The above copyright notice and this permission notice shall be included in all
 # copies or substantial portions of the Software.
 # 
 # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 # FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 # COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 # IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 # CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 #}
<!DOCTYPE gdml>
<gdml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation="http://service-spi.web.cern.ch/service-spi/app/releases/GDML/schema/gdml.xsd">
    <define>
        {% include '01_defines.tgdml' %}
        
        <variable name="NLayers"        value="48" />
        <variable name="NSideSells"     value="3" />
        <variable name="BoxSide_mm"     value="600" />
        <variable name="ScnSizeX_HCAL_mm"    value="192" />
        <variable name="ScnSizeY_HCAL_mm"    value="194" />
        <variable name="ScnWidth_mm"    value="4" />
        <variable name="CnvWidth_mm"    value="25" />
        <variable name="CellWidth_mm"   value="2*ScnWidth_mm" />
        <variable name="LayerWidth_mm"  value="CellWidth_mm + CnvWidth_mm" />
        <variable name="SizeX_HCAL_mm"       value="BoxSide_mm" />
        <variable name="SizeY_HCAL_mm"       value="BoxSide_mm" />
        <variable name="SizeZ_HCAL_mm"       value="LayerWidth_mm*NLayers" />
        
        {#
        <position name="CnvOffset"    x="0"    y="0" z="-CnvWidth_mm/2"                         unit="mm" />
        <position name="ScnBoxOffset" x="0"    y="0" z="-(CnvWidth_mm + CellWidth_mm/2)"        unit="mm" />
        <position name="AllBoxOffset" x="0"    y="0" z="-(CnvWidth_mm + CellWidth_mm)"          unit="mm" />
        <position name="Scn1Offset"   x="204"  y="9" z="-(CnvWidth_mm + ScnWidth_mm/2)"         unit="mm" />
        <position name="Scn4Offset"   x="12"   y="9" z="-(CnvWidth_mm + ScnWidth_mm + ScnWidth_mm/2)" unit="mm" />
        <position name="Scn7Offset"   x="-180" y="9" z="-(CnvWidth_mm + ScnWidth_mm/2)"         unit="mm" />
        #}

        <position name="CnvOffset"    x="0"    y="0" z="-CellWidth_mm/2"     unit="mm" />
        <position name="ScnBoxOffset" x="0"    y="0" z=" CnvWidth_mm/2"      unit="mm" />
        <position name="Scn1Offset"   x="204"  y="9" z="(CnvWidth_mm-ScnWidth_mm)/2"     unit="mm" />
        <position name="Scn4Offset"   x="12"   y="9" z="(CnvWidth_mm+ScnWidth_mm)/2"     unit="mm" />
        <position name="Scn7Offset"   x="-180" y="9" z="(CnvWidth_mm-ScnWidth_mm)/2"     unit="mm" />

    {#    <position name="AllScnOffset" x="12" y="9" z="-(ScnWidth_mm)" unit="mm" /> #}
        
    </define>
    
    <materials>
        {% include '02_materials.tgdml' %}
    </materials>

    <solids>
        {# include '03_solids.tgdml' #}

        <box name="geoHCAL"     x="BoxSide_mm"
                                y="BoxSide_mm"
                                z="48*(CnvWidth_mm + CellWidth_mm)" />
        
        <box name="geoLayer"    x="BoxSide_mm"
                                y="BoxSide_mm"
                                z="CnvWidth_mm + CellWidth_mm" />
        
        <box name="geoCnv"      x="BoxSide_mm"
                                y="BoxSide_mm"
                                z="CnvWidth_mm" />
        
        <box name="geoScn1"     x="ScnSizeX_HCAL_mm"
                                y="ScnSizeY_HCAL_mm"
                                z="ScnWidth_mm" />
        
        <box name="geoScn4"     x="ScnSizeX_HCAL_mm"
                                y="ScnSizeY_HCAL_mm"
                                z="ScnWidth_mm" />
        
        <box name="geoScn7"     x="ScnSizeX_HCAL_mm"
                                y="ScnSizeY_HCAL_mm"
                                z="ScnWidth_mm" />
        
        <box name="geoScnCell"  x="ScnSizeX_HCAL_mm"
                                y="ScnSizeY_HCAL_mm"
                                z="ScnWidth_mm" />
                                
        <box name="geoScnLayer1" x="ScnSizeX_HCAL_mm"
                                 y="3*ScnSizeY_HCAL_mm"
                                 z="ScnWidth_mm"/>
                        
        <box name="geoScnLayer2" x="ScnSizeX_HCAL_mm"
                                 y="3*ScnSizeY_HCAL_mm"
                                 z="ScnWidth_mm"/>
        
        <box name="geoScnLayer3" x="ScnSizeX_HCAL_mm"
                                 y="3*ScnSizeY_HCAL_mm"
                                 z="ScnWidth_mm"/>
                                 
        <box name="geoAllScn"   x="3*ScnSizeX_HCAL_mm"
                                y="3*ScnSizeY_HCAL_mm"
                                z="2*ScnWidth_mm"/>
        
        <box name="geoScnBox"   x="SizeX_HCAL_mm"
                                y="SizeY_HCAL_mm"
                                z="2*ScnWidth_mm"/>
                                
        <box name="geoAllBox"   x="SizeX_HCAL_mm"
                                y="SizeY_HCAL_mm"
                                z="(CnvWidth_mm + 2*ScnWidth_mm)"/>
        
    </solids>
    
    <structure>
        
        <volume name="Scn1">
            <materialref ref="mtPlasticScintillator"/>
            <solidref ref="geoScnCell"/>
            <auxiliary auxtype="Visibility" auxvalue="wireframe"/>
        </volume>
        
        <volume name="Scn4">
            <materialref ref="mtPlasticScintillator"/>
            <solidref ref="geoScnCell"/>
            <auxiliary auxtype="Visibility" auxvalue="wireframe"/>
        </volume>
        
        <volume name="Scn7">
            <materialref ref="mtPlasticScintillator"/>
            <solidref ref="geoScnCell"/>
            <auxiliary auxtype="Visibility" auxvalue="wireframe"/>
        </volume>
        
        <volume name="ScnLayer1">
            <materialref ref="mtPlasticScintillator"/>
            <solidref ref="geoScnLayer1"/>
            <replicavol number="3">
                <volumeref ref="Scn1"/>
                <replicate_along_axis>
                    <direction y="1"/>
                    <width  value="ScnSizeY_HCAL_mm"   unit="mm"/>
                    <offset value="0"              unit="mm"/>
                </replicate_along_axis>
            </replicavol>
            <auxiliary auxtype="Visibility" auxvalue="false"/>
        </volume>
        
        <volume name="ScnLayer2">
            <materialref ref="mtPlasticScintillator"/>
            <solidref ref="geoScnLayer2"/>
            <replicavol number="3">
                <volumeref ref="Scn4"/>
                <replicate_along_axis>
                    <direction y="1"/>
                    <width  value="ScnSizeY_HCAL_mm"   unit="mm"/>
                    <offset value="0"              unit="mm"/>
                </replicate_along_axis>
            </replicavol>
            <auxiliary auxtype="Visibility" auxvalue="false"/>
        </volume>
        
        <volume name="ScnLayer3">
            <materialref ref="mtPlasticScintillator"/>
            <solidref ref="geoScnLayer3"/>
            <replicavol number="3">
                <volumeref ref="Scn7"/>
                <replicate_along_axis>
                    <direction y="1"/>
                    <width  value="ScnSizeY_HCAL_mm"   unit="mm"/>
                    <offset value="0"              unit="mm"/>
                </replicate_along_axis>
            </replicavol>
            <auxiliary auxtype="Visibility" auxvalue="false"/>
        </volume>
    {#    
        <volume name="AllScn">
            <materialref ref="mtPlasticScintillator"/>
            <solidref ref="geoAllScn"/>
            <physvol>
                <volumeref ref="ScnLayer1"/>
                <positionref ref="Scn1Offset"/>
            </physvol>
            <physvol>
                <volumeref ref="ScnLayer2"/>
                <positionref ref="Scn4Offset"/>
            </physvol>
            <physvol>
                <volumeref ref="ScnLayer3"/>
                <positionref ref="Scn7Offset"/>
            </physvol>
            <auxiliary auxtype="Visibility" auxvalue="false"/>
        </volume>
        #}
        <volume name="ScnBox">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoScnBox" />
            <auxiliary auxtype="Visibility" auxvalue="wireframe"/>
        </volume>
        
        <volume name="Cnv">
            <materialref ref="mtFe"/>
            <solidref ref="geoCnv" />
            <auxiliary auxtype="Visibility" auxvalue="wireframe"/>
        </volume>
        
        <volume name="AllBox">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoAllBox"/>
            {#
            <physvol>
                <volumeref ref="AllScn"/>
                <positionref ref="AllScnOffset"/>
            </physvol>
            #}
            <physvol>
                <volumeref ref="Cnv" />
                <positionref ref="CnvOffset" />
            </physvol>
            {#<physvol>
                <volumeref ref="ScnBox"/>
                <positionref ref="ScnBoxOffset"/>
            </physvol>#} 
            <physvol>
                <volumeref ref="ScnLayer1"/>
                <positionref ref="Scn1Offset"/>
            </physvol>
            <physvol>
                <volumeref ref="ScnLayer2"/>
                <positionref ref="Scn4Offset"/>
            </physvol>
            <physvol>
                <volumeref ref="ScnLayer3"/>
                <positionref ref="Scn7Offset"/>
            </physvol>       
            <auxiliary auxtype="Visibility" auxvalue="false"/>
        </volume>
        
        <volume name="HCAL">
            <materialref ref="G4_AIR"/>
            <solidref ref="geoHCAL"/>
            <replicavol number="48"> 
                <volumeref ref="AllBox"/>
                <replicate_along_axis>
                    <direction z="1"/>
                    <width  value="CnvWidth_mm + CellWidth_mm"   unit="mm"/>
                    <offset value="0"              unit="mm"/>
                </replicate_along_axis>
            </replicavol>
            <auxiliary auxtype="Visibility" auxvalue="false"/>
        </volume>
        
{#        
        <volume name="Layer">
            <solidref ref="geoLayer" />
            <physvol>
                <volumeref ref="Cnv" />
                <positionref ref="CnvOffset" />
            </physvol>
            <physvol>
                <volumeref ref="AllBox" />
                <positionref ref="AllBoxOffset" />
            </physvol>
            <auxiliary auxtype="Visibility" auxvalue="false"/>
        </volume>    #}
        
    </structure>
    
    <setup name="Default" version="1.0" >
        <world ref="HCAL" />
    </setup>
    <setup name="StandaloneLayer" version="1.0" >
        <world ref="AllBox" />
    </setup>
</gdml>
