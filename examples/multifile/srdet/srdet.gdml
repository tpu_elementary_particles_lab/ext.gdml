<?xml version="1.0" encoding="UTF-8" ?>

<!--
 Copyright (c) 2016 Bogdan Vasilishin <togetherwithra@gmail.com>
 Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-->

<!DOCTYPE gdml [
    <!ENTITY defines    SYSTEM "../01_defines.igdml">
    <!ENTITY materials  SYSTEM "../02_materials.igdml">
    <!ENTITY solids     SYSTEM "../03_solids.igdml">
]>

<gdml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation="http://service-spi.web.cern.ch/service-spi/app/releases/GDML/schema/gdml.xsd">
    
    <define>
        &defines;

    </define>

    <materials>
        &materials;
    </materials>

    <solids>
        &solids;
        
        <box name="geo_entire_srdet"            x="srdet_XSide"     y="srdet_YSide"     z="srdet_Length" />
        <box name="geo_srdet_YLayer"            x="srdet_XSide"     y="srdet_CellSide"  z="srdet_Length" />
        <!-- Horizontal layers are included in vertical layers -->
        <box name="geo_srdet_XLayer"            x="srdet_CellSide"  y="srdet_CellSide"  z="srdet_Length" />
        <!-- Simple sandwitch-like layer includes Lead and Scintilator layers-->
        <box name="geo_srdet_Layer"             x="srdet_CellSide"  y="srdet_CellSide"  z="srdet_LayerWidth" />
        <box name="geo_srdet_PbLayer"           x="srdet_CellSide"  y="srdet_CellSide"  z="srdet_PbWidth" />
        <box name="geo_srdet_ScLayer"           x="srdet_CellSide"  y="srdet_CellSide"  z="srdet_ScWidth" />
    </solids>

    <structure>
        
        <volume name="srdet_PbLayer">
            <materialref    ref="mtLead"/>
            <solidref       ref="geo_srdet_PbLayer"/>
            <auxiliary      auxtype="style"     auxvalue="Default:!hidden/StandaloneCell:#8f8f8fff,!surface/Detailed:!hidden" />
        </volume>

        <volume name="srdet_ScLayer">
            <materialref    ref="G4_PLASTIC_SC_VINYLTOLUENE"/>
            <solidref       ref="geo_srdet_ScLayer"/>
            <auxiliary      auxtype="style"     auxvalue="Default:!hidden/StandaloneCell:#0f80ffff,!surface/Detailed:!hidden" />
            <auxiliary      auxtype="sensDet"   auxvalue="test01_SD:/${PROJ}det/srdet"/>
        </volume>

        <volume name="srdet_Layer">
            <materialref    ref="G4_AIR"/>
            <solidref       ref="geo_srdet_Layer" />

            <physvol>
                <volumeref      ref="srdet_PbLayer"/>
                <positionref    ref="PbLayerOffset"/>
            </physvol>

            <physvol>
                <volumeref      ref="srdet_ScLayer"/>
                <positionref    ref="ScLayerOffset"/>
            </physvol>

            <auxiliary      auxtype="style" auxvalue="Default:!hidden/StandaloneCell:!wireframe/Detailed:!hidden" />
        </volume>

        <volume name="srdet_XLayer">
            <materialref    ref="G4_AIR"/>
            <solidref       ref="geo_srdet_XLayer"/>
            <replicavol     number="srdet_NLayers" >
                <volumeref      ref="srdet_Layer"/>
                <replicate_along_axis>
                    <direction      z="1"/>
                    <width          value="srdet_LayerWidth_mm"    unit="mm" />
                    <offset         value="0"                   unit="mm"/>
                </replicate_along_axis>
            </replicavol>
            <auxiliary      auxtype="style" auxvalue="Default:!hidden/Detailed:!wireframe" />
        </volume>

        <volume name="srdet_YLayer">
            <materialref    ref="G4_AIR"/>
            <solidref       ref="geo_srdet_YLayer" />
            <replicavol     number="srdet_NXCells">
                <volumeref      ref="srdet_XLayer"/>
                <replicate_along_axis>
                    <direction      x="1"/>
                    <width          value="srdet_CellSide_mm"   unit="mm" />
                    <offset         value="0"                   unit="mm" />
                </replicate_along_axis>
            </replicavol>
            <auxiliary      auxtype="style" auxvalue="Default:!hidden/Detailed:!wireframe" />
        </volume>
       
        <volume name="entire_srdet">
            <materialref    ref="G4_AIR" />
            <solidref       ref="geo_entire_srdet" />
            <replicavol     number="srdet_NYCells">
                <volumeref      ref="srdet_YLayer" />
                <replicate_along_axis>
                    <direction      y="1" />
                    <width          value="srdet_CellSide_mm"      unit="mm" />
                    <offset         value="0"                   unit="mm" />
                </replicate_along_axis>
            </replicavol>
            <auxiliary      auxtype="style" auxvalue="Default:!wireframe/Detailed:!wireframe" />
        </volume>
    
    </structure>
    
    <setup name="Default" version="1.0" >
        <world ref="entire_srdet" />
    </setup>

    <!-- Wireframe, demonstrates, that the geometry is really segmentated -->
    <setup name="Detailed" version="1.0" >
        <world ref="entire_srdet" />
    </setup>
    <!-- Use this one for standalone view of ECAL cell -->
    <setup name="StandaloneCell" version="1.0" >
        <world ref="srdet_Layer" />
    </setup>

</gdml>
