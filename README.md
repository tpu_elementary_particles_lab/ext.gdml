<pre>
             ___________  ______________________ ___________   ________
        ====/  ____/|  |=/  //__   ___/  ___   //  __   \  |==/   //  /=
        ===/  /=====|  |/  /== /  /==/  /==/__//  /==|  /  |=/   //  /==
        ==/  _____/=/     |===/  /==/  /======/  /==/  /   |/   //  /===
        =/  /======/  /|  |==/  /==/  /==/    \ /==/  / /=\_/  //  /====
        /_______/=/__/=|__| /__/__/________/__/______/_/===/__//_______/
</pre>

# extGDML package

`extGDML` comes from "extensible GDML" and refers to
[GDML --- Geometry Description Markup Language](http://gdml.web.cern.ch/GDML/)
format initially developed inside the
[Geant4](http://geant4.web.cern.ch/geant4/) framework.

Its main purpose is to provide strict and extensible GDML parser
converting GDML descriptions in various formats supporting auxilliary
information about sensitive dedectors, event display digits, etc.

`extGDML` is written on Python and utilizes
[Dave Kuhlman's generateDS](http://www.davekuhlman.org/generateDS.html) parser
generator.

It is distributed under the MIT license.

## Few words about aim of GDML

The concept of extensible geometry-descripiton language based on strict
XSD schema definition was very promising since it allows to keep all the
setup information at one place.

This format could be used as a interim format for data exchange between
various software frameowrks and libraries. It still can be directly edited
by humans. Moreowver, the strict XSD schema definition claims the internal
structure of this documents in a form available for various code-generation
software and many of the errors and incompatibilities can be found at early
stages of development.

## GDML extensions for HEP

As for high-energy physics experiments support, the
 1. Geometrical descriptions of detectors and media for Monte-Carlo simulation,
 2. Detector placement information,
 3. Particle tracking groups and vertex points,
 4. Event-deisplay digits,
and other auxilliary information about positions, rotations, quantity
definitions, constraints, sensetive detector information, etc. can be
well-defined by utilizing a special form of XML documents combining all the
things in format that is well-redable by humans and machine.

So far, however, the community developed quite few software dealing
with GMDL. Namely, this format is fully supported only by Geant4 framework for
which this format was initially developed.

Speaking about HEP, the [ROOT](https://root.cern.ch/) framework is supposed to
be the major consumer of GDML. The original ROOT parser is based
on their own XML parsing library and was implemented in a rudimentary and
archaic way supporting GDML features only selectively. Moreover, this parser
suffers from design restrictions and can not be easily extended (all the tags
are hardcoded, the comparison is done via `strcmp()`).

Other parts of ROOT framework is widely used in HEP: it offers a lot of
convinient tools for data analysis and representation. Its internal geometrical
format comes from Geant3 and offers all the entities existing in GDML so they
sometimes can be just directly mapped.

## extGDML structure

`extGDML` comes with Makefile requiring `generateDS` and `SWIG` to be
installed. They are needed to generate Python parser and CLHEP-evaluator
extensions which then become a part of `extGDML/` package. The native CLHEP
evaluator must be installed as a library in order to provide the native
environment for evaluating GDML arithmetical expressions. See `INSTALL` for
details about how to build these parts.

The available exporting back-ends are located inside the package directory
(`extGDML/`) and represented as a sub-directories containing the following
files corresponding to major GDML sections:

    $ tree ./extGDML/root/
    ./extGDML/root/
    ├── _01_definitions.py
    ├── _02_materials.py
    ├── _03_solids.py
    ├── _04_structure.py
    ├── _05_setup.py
    ... (other files are auxilliary modules for exporting back-ends)

The particular interfaces for these modules are to be documented. However the
major logic of conversion is based upon sequential evaluation of `read_foo`
routine inside the `_0?_foo.py` module.

