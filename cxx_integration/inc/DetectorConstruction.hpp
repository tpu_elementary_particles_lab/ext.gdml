/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_EXTGDML_COMMON_DETECTOR_CONSTRUCTION_H
# define H_EXTGDML_COMMON_DETECTOR_CONSTRUCTION_H

# include "config.h"

# include <G4VUserDetectorConstruction.hh>

class G4VPhysicalVolume;

namespace extGDML {

/// Detector construction allowing to use the geometry read from the GDML file.
class DetectorConstruction : public G4VUserDetectorConstruction {
private:
    /// World pointer to be set in case of external world construction.
    G4VPhysicalVolume * _worldPtr;
public:
    DetectorConstruction( G4VPhysicalVolume * setWorld = nullptr );

    /// Mandatory Geant interfacing method.
    virtual G4VPhysicalVolume * Construct() override;
};

}  // namespace extGDML

# endif  // H_EXTGDML_COMMON_DETECTOR_CONSTRUCTION_H

