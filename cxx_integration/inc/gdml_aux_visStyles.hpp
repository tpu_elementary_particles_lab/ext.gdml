/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_EXT_GDML_AUX_VIS_STYLES_H
# define H_EXT_GDML_AUX_VIS_STYLES_H

# include "config.h"
# include "auxInfoSet.hpp"

# include <regex>
# include <map>
# include <list>

namespace extGDML {

class StyleParser {
public:
    static std::string
                srxColorBase,
                srxColorStr,
                srxVisibilityStr,
                srxSetupName,
                srxGroupedStyle,
                srxOverall
                ;
    static std::regex rx,
                      rxSN,
                      rxClr,
                      rxVis
                      ;

    struct Style {
        enum DrawingStyle : uint8_t {
            unassigned = 0,
            hidden = 1,
            wireframe = 2,
            surface = 3,
        } drawingStyle;
        union ColorCode {
            struct { uint8_t r,g,b,a;  } _;
            uint8_t byte[4];
        } colorCode;
        void apply( G4LogicalVolume * const tVol );
    };
protected:
    typedef std::map<std::string, Style *> VolumeStylesDict;
    Style _defaultStyle;
    std::map< G4LogicalVolume *, VolumeStylesDict > _dicts;
    std::list<Style> _stylesStorage;
private:
    StyleParser() = delete;
public:
    StyleParser( const Style & dft ) : _defaultStyle(dft) {}
    ~StyleParser(){}

    static Style::ColorCode parse_color_code_rgb(  const std::string & );
    static Style::ColorCode parse_color_code_rgba( const std::string & );
    static void parse_styles(
            std::map<std::string, Style *> &,
            std::list<Style> &,
            const std::string & );
    /// Returns `true' if str satisfies selector.
    static bool setup_selector_match( const std::string & str, const std::string & selector );

    /// Parses style string and associates it for certain volume.
    void load_vis_styles_for_volume( G4LogicalVolume *, const std::string & );

    /// Applies parsed styles for all volumes according to setup string.
    void apply_styles_for( const std::string & );
};

namespace extras {
/// Visibility styles. See gdml_aux_visStyles.cpp for implementation details.
void apply_styles_selector( const std::string & );
}  // namespace extras

}  // namespace extGDML

# endif  // H_EXT_GDML_AUX_VIS_STYLES_H
