/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_GEANT4_AUX_INFO_PROCESSOR_H
# define H_GEANT4_AUX_INFO_PROCESSOR_H

# include "config.h"

# include <map>

class G4LogicalVolumeStore;
class G4LogicalVolume;
class G4GDMLParser;
class G4String;

namespace extGDML {

/**@class GDMLAuxInfoProcessor
 * @brief Implements auxiliary/userinfo tag postprocessing for P348 GDML files.
 *
 * This now is the form of dictionary with pre-implemented loop, reading
 * tags info and applying a certain routines for each found entry.
 *
 * This class instance implements processing of additional information
 * supplied in logical volume's `<auxilliary>` and `<userinfo>` tags in
 * GDML files related to p348 experiment modelling stuff.
 *
 * TODO: <userinfo> tag is supported only by GDML schema 3.1.2 and still
 * not by the Geant4's GDML parser.
 */
class GDMLAuxInfoProcessor {
public:
    typedef void (*PropertyTreatmentCallback)(G4LogicalVolume*,
                  const G4String & value);
private:
    /// Actually a wrapper to singleton std::vector<G4LogicalVolume*>.
    G4LogicalVolumeStore * _logStoreCachePtr;

    std::map<std::string, PropertyTreatmentCallback> _callbacks;

    static GDMLAuxInfoProcessor * _self;
protected:
    GDMLAuxInfoProcessor();
public:
    virtual ~GDMLAuxInfoProcessor();

    static GDMLAuxInfoProcessor & self();
    static const GDMLAuxInfoProcessor & const_self();

    /// Registers particular callback.
    void add_processing_routine( const std::string & name,
                                 PropertyTreatmentCallback cllb );

    /// Usually should be called only once, after RunManager initialized. 
    void apply( G4GDMLParser & );
};  // class P348GDMLAuxInfoProcessor
}  // namespace extGDML

/*
 * Dictionary carrying in macros.
 * Example for auxinfo type 'color':
 *
 * GDML_AUXINFO_PROCESSOR_BGN( color, volume, strVal ) {
 *     // G4LogicalVolume * volume;
 *     // const G4String & strVal;
 *     // ... do something with variables volume, strVal ...
 * } GDML_AUXINFO_PROCESSOR_END( color )
 *
 */


# define GDML_AUXINFO_PROCESSOR_BGN( typeName, volPtrName, valStringName )  \
void __ ## typeName ## _dict_push()  __attribute__(( constructor(156) ));   \
void __GDML_ ## typeName( G4LogicalVolume * volPtrName,                     \
                          const G4String & valStringName )

# define GDML_AUXINFO_PROCESSOR_END( typeName )                             \
void __ ## typeName ## _dict_push() {                                       \
    extGDML::GDMLAuxInfoProcessor::self().add_processing_routine(              \
        # typeName,                                                         \
        __GDML_ ## typeName );                                              \
}

# endif  // H_GEANT4_AUX_INFO_PROCESSOR_H

