/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_EXT_GDML_ERROR_HANDLING_H
# define H_EXT_GDML_ERROR_HANDLING_H

# include <stdexcept>

namespace extGDML {

/**@class Exception
 * @brief An extGDML package exception class.
 *
 * This class implements an exception used in ext.GDML C++ library. One
 * probably will not need to deal with this class directly outside of
 * exception catching. For any third-party software providing user's
 * error-handling fallbacks one may be interested in overriding the 
 * `extgdml_error_handle` ptr that is always invoked instead of direct
 * instance construction.
 * */
class Exception : public std::runtime_error {
public:
    enum Code {
        commonRuntime,
        notFound,
        invalidExpression,
        // ...
    };
private:
    Code _code;
protected:
    Exception( const std::string & );
    Exception( Code, const std::string & );
public:
    static void native_handle( Code, const std::string & );
    Code code() const throw() { return _code; }
};

# define EXT_GDML_ERR( code, fmt, ... )                                     \
do { char bf[256]; snprintf(bf, sizeof(bf), fmt, ## __VA_ARGS__ );          \
  ::extGDML::extgdml_error_handle( ::extGDML::Exception:: code, bf );       \
  } while(false);

/// Common type for exception handling in ext.GDML lib:
typedef void (*ExceptionHandler)(Exception::Code, const std::string &);

/// This function pointer should refer to callback throwing an exception.
/// One may set it to custom handle in order to provide their own
/// error-handling implementation.
extern ExceptionHandler extgdml_error_handle;

}  // namespace extGDML

# endif  // H_EXT_GDML_ERROR_HANDLING_H

