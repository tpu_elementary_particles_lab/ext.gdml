/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_EXTGDML_AUXINFO_SET_H
# define H_EXTGDML_AUXINFO_SET_H

# include "config.h"

# include <unordered_map>
// # include <unordered_set>
# include <vector>

class G4LogicalVolumeStore;
class G4LogicalVolume;
class G4GDMLParser;
class G4String;

namespace extGDML {

/**@class AuxInfoSet
 * @brief Container for auxiliary/userinfo processing.
 *
 * This is the form of dictionary with pre-implemented loop, reading
 * tags info and applying a certain routines for each found entry.
 *
 * This class instance implements processing of additional information
 * supplied in logical volume's `<auxilliary>` and `<userinfo>` tags in
 * GDML files related to experiment modelling stuff.
 *
 * todo: <userinfo> tag is supported only by GDML schema 3.1.2 and still
 * not by the default Geant4's GDML parser.
 */
class AuxInfoSet {
public:
    /// Callback type that is to be invoked on particular <auxilliary/> tag
    /// name. The corresponding volume will be given as a first argument, the
    /// string containing value will be provided as a second.
    typedef void (*PropertyTreatmentCallback)(
                        G4LogicalVolume*,
                        const G4String & value,
                        std::ostream & warnStream );
    /// 
    typedef std::unordered_map<std::string, PropertyTreatmentCallback>
            AuxInfoIndex;
private:
    /// Actually a wrapper to singleton std::vector<G4LogicalVolume*>.
    //G4LogicalVolumeStore * _logStoreCachePtr;
    /// Class member containing callbacks.
    AuxInfoIndex _callbacks;
    /// Common dictionary of auxinfo processing callbacks implemented in
    /// system.
    static AuxInfoIndex * _index;
public:
    AuxInfoSet();
    AuxInfoSet( const std::vector<std::string> & );
    virtual ~AuxInfoSet();

    /// Pushes callback in this instance's map.
    void enable_auxinfo_tag( const std::string & name );

    /// Usually should be called only once, after RunManager initialized. 
    /// If the volume store is set to NULL, the G4's system 
    /// G4LogicalVolumeStore singleton will be used.
    /// If warning stream pointer is NULL, the std::cerr will be used to
    /// indicate the warnings.
    void apply( G4GDMLParser &,
                G4LogicalVolumeStore * volStore=nullptr,
                std::ostream * warnStreamPtr=nullptr );

    /// Registers particular callback. One need to use this function for manual
    /// registiring their own callbacks by providing its name and function
    /// pointer.
    static void register_callback( const std::string & name,
                                   PropertyTreatmentCallback cllb );

    /// Prints out list of registered tags into given output stream.
    static void list_registered_tags( std::ostream & );
};  // class AuxInfoSet
}  // namespace extGDML

/*
 * Dictionary carrying in macros.
 * Example for auxinfo type 'color':
 *
 * EXTGDML_AUXINFO_PROCESSOR_BGN( color, volume, strVal, myCErr ) {
 *     // G4LogicalVolume * volume;
 *     // const G4String & strVal;
 *     // ... do something with variables volume, strVal ...
 * } EXTGDML_AUXINFO_PROCESSOR_END( color )
 *
 */

# define EXTGDML_AUXINFO_PROCESSOR_BGN( typeName, volPtrName,               \
                                        valStringName, wStreamName )        \
void __ ## typeName ## _dict_push()  __attribute__(( constructor(156) ));   \
void __GDML_ ## typeName( G4LogicalVolume * volPtrName,                     \
                          const G4String & valStringName,                   \
                          std::ostream & wStreamName )

# define EXTGDML_AUXINFO_PROCESSOR_END( typeName )                          \
void __ ## typeName ## _dict_push() {                                       \
    extGDML::AuxInfoSet::register_callback(                                 \
        # typeName,                                                         \
        __GDML_ ## typeName );                                              \
}

# endif  // H_EXTGDML_AUXINFO_SET_H

