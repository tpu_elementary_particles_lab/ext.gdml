/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "FieldDict.hpp"
# include <G4Field.hh>

namespace extGDML {

FieldDictionary * FieldDictionary::_self = nullptr;

FieldDictionary &
FieldDictionary::self(){
    if( !_self ) {
        _self = new FieldDictionary();
    }
    return *_self;
}

FieldDictionary::~FieldDictionary() {}

void
FieldDictionary::register_field_instance( const std::string & name,
                                          FieldCtr fieldPtr ) {
    _dict[name] = fieldPtr;
}

FieldDictionary::FieldCtr
FieldDictionary::operator[]( const std::string & name ) const {
    auto it = _dict.find( name );
    if( _dict.end() == it ) {
        std::cerr << "While constructing field named " << name << std::endl;
        throw std::range_error( "Named field constructor not found." );
    }
    return it->second;
}

void
FieldDictionary::print_field_list( std::ostream & os ) const {
    for ( auto it = _dict.begin(); it != _dict.end(); ++it) {
        os << "\t" << it->first << std::endl;
    }
}

}  // namespace extGDML

