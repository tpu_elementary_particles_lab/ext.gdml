/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "gdml_aux_visStyles.hpp"

# include "auxInfoSet.hpp"
# include "extras.hpp"
# include "errors.hpp"
# include "logging.hpp"

# include <G4VisAttributes.hh>
# include <G4LogicalVolume.hh>

# include <regex>
# include <cstdlib>
# include <ostream>

# if 1
namespace extGDML {

static StyleParser * _static_defaultStylesParser = nullptr;  // class StyleParser

std::string
    StyleParser::srxColorBase( "(?:[[:digit:]]|[a-f])" ),
    StyleParser::srxColorStr( "(#(?:" + srxColorBase + "{6}" + "|" + srxColorBase + "{8}))" ),
    StyleParser::srxVisibilityStr( "(!(?:surface|wireframe|hidden))" ),
    StyleParser::srxSetupName( "((?:(?:[[:alpha:]][[:alnum:]]*)|\\*)|(?:(?:<|>)[0-9]))" ),
    StyleParser::srxGroupedStyle = "(?:" 
                                        "(?:" + srxSetupName +  ":" + srxColorStr + "(?:," + srxVisibilityStr + ")?)|" +
                                        "(?:" + srxSetupName +  ":" + srxVisibilityStr + "(?:," + srxColorStr + ")?)|"
                                  ")",
    StyleParser::srxOverall = srxGroupedStyle + "(?:/" + srxGroupedStyle + ")*"
    ;
std::regex
    StyleParser::rx(srxOverall),
    StyleParser::rxSN(srxSetupName),
    StyleParser::rxClr(srxColorStr),
    StyleParser::rxVis(srxVisibilityStr)
    ;

std::ostream &
operator<<( std::ostream & os, const StyleParser::Style & style ) {
    char bf[64];
    snprintf( bf, 64, "#%02x%02x%02x%02x,%s",
            (int) style.colorCode.byte[0],
            (int) style.colorCode.byte[1],
            (int) style.colorCode.byte[2],
            (int) style.colorCode.byte[3],
            ( style.drawingStyle == StyleParser::Style::hidden ? "!hidden" : (
                style.drawingStyle == StyleParser::Style::wireframe ? "!wireframe" : "!surface") )
        );
    os << bf;
    return os;
}

StyleParser::Style::ColorCode
StyleParser::parse_color_code_rgb( const std::string & seq ) {
    uint32_t hexRepr = strtol(seq.substr(1).c_str(), nullptr, 16);
    uint8_t r = ((hexRepr >> 16) & 0xFF),
            g = ((hexRepr >> 8 ) & 0xFF),
            b = ((hexRepr      ) & 0xFF);
    return {{r, g, b, 0xff}};
}

StyleParser::Style::ColorCode
StyleParser::parse_color_code_rgba( const std::string & seq ) {
    uint32_t hexRepr = strtol(seq.substr(1).c_str(), nullptr, 16);
    uint8_t r = ((hexRepr >> 24) & 0xFF),
            g = ((hexRepr >> 16) & 0xFF),
            b = ((hexRepr >> 8 ) & 0xFF),
            a = ((hexRepr      ) & 0xFF);
    return {{r, g, b, a}};
}

void
StyleParser::parse_styles( std::map<std::string, Style *> & stylesDict,
                           std::list<Style> & storage,
                           const std::string & s ) {
    std::smatch sm;
    std::string setupName;  // can hold >/</= expr also
    StyleParser::Style style;
    if( _static_defaultStylesParser ) {
        style = _static_defaultStylesParser->_defaultStyle;
    }

    if(std::regex_match( s, sm, rx )) {
        for(size_t i = 0; i < sm.size(); ++i) {
            std::ssub_match sub_match = sm[i];
            std::string piece = sub_match.str();
            if(piece.empty()) continue;

            if( std::regex_match( piece, rxSN ) ) {
                if( !setupName.empty() ) {  // not a first entry
                    # if 1
                    std::stringstream ss;
                    ss << style;
                    std::string styleBackString = ss.str();
                    extGDML_info( "Style(s) for selector `%s' processed: `%s' "
                                  "from string `%s'.\n",
                            setupName.c_str(),
                            styleBackString.c_str(),
                            s.c_str() );
                    # endif
                    // push back previous, as it supposingly next one
                    storage.push_back(style);
                    stylesDict[setupName] = &(storage.back());
                }
                // initialize new style
                bzero( &style, sizeof(StyleParser::Style) );
                if( _static_defaultStylesParser ) {
                    style = _static_defaultStylesParser->_defaultStyle;
                }
                setupName = piece;
            } else if( std::regex_match( piece, rxClr ) ) {
                if( 7 == piece.size() ) {  // rgb
                    style.colorCode = StyleParser::parse_color_code_rgb(  piece );
                } else {  // rgba
                    style.colorCode = StyleParser::parse_color_code_rgba( piece );
                }
            } else if( std::regex_match( piece, rxVis ) ) {
                if( 'h' == piece[1] ) {
                    style.drawingStyle = StyleParser::Style::hidden;
                } else if( 'w' == piece[1] ) {
                    style.drawingStyle = StyleParser::Style::wireframe;
                } else if( 's' == piece[1] ) {
                    style.drawingStyle = StyleParser::Style::surface;
                } else {
                    EXT_GDML_ERR( invalidExpression,
                                  "Regex error: unknown visibility "
                                  "specifier: %s",
                                  piece.c_str() );
                }
            } else if(std::regex_match( piece, rx )) {
                // omit entire string -- ok
            } else {
                EXT_GDML_ERR( invalidExpression, "Regex error: can not "
                              "interpret a valid string: %s", piece.c_str() );
            }
        }   
    } else {
        EXT_GDML_ERR( invalidExpression,
                      "Style string invalid: \"%s\".",
                      s.c_str() );
    }
    if( !setupName.empty() ) {  // not a first entry
        //p348g4_log3( "Style(s) for setup '%s' processed.\n", setupName.c_str() );
        # if 1
        std::stringstream ss;
        ss << style;
        std::string styleBackString = ss.str();
        extGDML_info( "Style(s) for finalizing selector `%s' processed: `%s' "
                      "from string `%s'.\n",
                setupName.c_str(),
                styleBackString.c_str(),
                s.c_str() );
        # endif
        storage.push_back(style);
        stylesDict[setupName] = &(storage.back());
    } else {
        EXT_GDML_ERR( notFound,
            "Couldn't recognize setup name for style string invalid: \"%s\".",
            s.c_str() );
    }
}

void
StyleParser::Style::apply( G4LogicalVolume * const tVol ) {
    G4Colour clr( colorCode._.r/255., colorCode._.g/255.,
                  colorCode._.b/255., colorCode._.a/255. );
    G4VisAttributes va(clr);
    if( hidden == drawingStyle ) {
        va.SetVisibility(false);
    } else if( wireframe == drawingStyle ) {
        va.SetVisibility(true);
        va.SetForceWireframe(true);
    } else if( surface == drawingStyle ) {
        va.SetForceSolid(true);
        va.SetVisibility(true);
    }
    //va.SetColour(clr);  // xxx?
    tVol->SetVisAttributes(va);
    # if 1  // XXX
    std::string s; {
        std::stringstream ss;
        ss << *this;
        s = ss.str();
    }
    extGDML_info( "Style %p:'%s' applied on volume %p:`%s'.\n",
              this,
              s.c_str(),
              tVol,
              tVol->GetName().c_str() );
    # endif
}

void
StyleParser::load_vis_styles_for_volume(
        G4LogicalVolume * volPtr,
        const std::string & stylesString ) {
    auto mapIt = _dicts.find( volPtr );
    VolumeStylesDict * dictPtr = nullptr;
    if( _dicts.end() == mapIt ) {
        _dicts[volPtr] = VolumeStylesDict();
        dictPtr = &(_dicts[volPtr]);
    } else {
        dictPtr = &(mapIt->second);
    }
    parse_styles( *dictPtr, _stylesStorage, stylesString );
}

void
StyleParser::apply_styles_for( const std::string & setupName ) {
    for( auto it  = _dicts.begin();
          _dicts.end() != it; ++it ) {
        bool thisVolumeApplied = false;
        for( auto iit  = it->second.begin();
                 it->second.end() != iit; ++iit ) {
            if( setup_selector_match( setupName, iit->first ) ) {
                //extGDML_info( "Setup name %s matches selector %s.\n",
                //    setupName.c_str(), iit->first.c_str() );
                iit->second->apply( it->first );
                thisVolumeApplied = true;
                break;  // apply first satisfying match
            }
        }
        if( !thisVolumeApplied ) {
            // try to apply '*' selector:
            auto iit = it->second.find("*");
            if( it->second.end() != iit ) {
                iit->second->apply( it->first );
            }
        }
    }
}

bool
StyleParser::setup_selector_match(
        const std::string & str,
        const std::string & selector) {
    bool numericalSelector = false;
    if( str[0] == '=' ||
        str[0] == '>' ||
        str[0] == '<' ) {
        numericalSelector = true;
    }
    if( !numericalSelector ) {
        return str == selector;
    } else {
        if( !isdigit(str[0]) ) {
            return false;
        }
    }
    uint8_t sltrLevel = (uint8_t) atoi( selector.substr(1).c_str() ),
             curLevel = (uint8_t) atoi( str.c_str() );
    if(      str[0] == '=' ) {
        return curLevel == sltrLevel;
    } else if( str[0] == '>' ) {
        return curLevel > sltrLevel;
    } else if( str[0] == '<' ) {
        return curLevel < sltrLevel;
    }
    return false;
}

/* Visual style notation:
 *
 *      stylesList ::= stylesForSetup
 *                   | stylesList '/' stylesForSetup
 *                   ;
 *
 *      stylesForSetup ::= modeSpec ':' HEXCOLOR
 *                       | modeSpec ':' HEXCOLOR ',' visibility
 *                       | modeSpec ':' visibility
 *                       ;
 *
 *      modeSpec ::= SETUPNAME
 *                 | '>' DIGIT
 *                 | '<' DIGIT
 *                 | '=' DIGIT
 *                 ;
 *
 *      visibility ::= '!wireframe'
 *                   | '!hidden'
 *                   | '!surface'
 *                   ;
 *
 *      HEXCOLOR    -- hex integer in form #rrggbb or #rrggbbaa.
 *      SETUPNAME   -- name of setup
 *      DIGIT       -- single 0..9 character
 *
 *  Note, that one can specify a 'verbosity level' instead of the
 *  setup name in form >/</= DIGIT where DIGIT is a number 0-9.
 */

EXTGDML_AUXINFO_PROCESSOR_BGN( style, volPtr, strVal, wStream ) {
    if( !_static_defaultStylesParser ) {
        extGDML_info("Creating styles parser...\n");
        StyleParser::Style dftStyle; {
            std::list<StyleParser::Style> tmpStorage;
            std::map<std::string, StyleParser::Style *> tmpIndex;
            StyleParser::parse_styles( tmpIndex, tmpStorage,
                STYLES_SELECTOR_DEFAULT_SETUP_NAME );
            dftStyle = *tmpIndex["dft"];
            {
                std::stringstream ss;
                ss << dftStyle;
                extGDML_info( "Default style for touchables is set now to `%s' "
                              "from spec `%s'.\n",
                    ss.str().c_str(),
                    STYLES_SELECTOR_DEFAULT_SETUP_NAME );
            }
        }
        _static_defaultStylesParser = new StyleParser(dftStyle);
    }
    StyleParser & sp = *_static_defaultStylesParser;
    sp.load_vis_styles_for_volume( volPtr, strVal );
} EXTGDML_AUXINFO_PROCESSOR_END( style )

namespace extras {
void
apply_styles_selector( const std::string & setupName ) {
    if( _static_defaultStylesParser ) {
        _static_defaultStylesParser->apply_styles_for( setupName );
        extGDML_info( "Styles selector `%s' applied.\n", setupName.c_str() );
    } else {
        extGDML_warn( "Styles parser uininitialized. Seems, there no styled "
                      "volumes to apply `%s' selector.\n",
                      setupName.c_str() );
    }
}
}  // namespace extras
}  // namespace extGDML
# endif

