/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "config.h"

# include "auxInfoSet.hpp"

# include <G4LogicalVolumeStore.hh>
# include <G4GDMLParser.hh>

namespace extGDML {

AuxInfoSet::AuxInfoIndex * AuxInfoSet::_index = nullptr;

AuxInfoSet::AuxInfoSet() {
    //_logStoreCachePtr(G4LogicalVolumeStore::GetInstance())
}

AuxInfoSet::AuxInfoSet( const std::vector<std::string> & names ) {
    if( !_index ) {
        throw std::runtime_error( "No auxilliary info processors were added"
            "while AuxInfoSet ctr was invoked with tag names provided." );
    }
    for( auto & name : names ) {
        enable_auxinfo_tag( name );
    }
}

AuxInfoSet::~AuxInfoSet() {
}

void
AuxInfoSet::enable_auxinfo_tag( const std::string & name ) {
    if( !_index ) {
        throw std::runtime_error( "No auxilliary info processors were added"
                            "while tag enabling requested in AuxInfoSet." );
    }
    auto cllbIt = _index->find( name );
    if( _index->end() == cllbIt ) {
        std::cerr << "Auxilliary tag procesor not registered: " << name
                  << std::endl
                  << "Available tags:" << std::endl
                  ;
        list_registered_tags( std::cerr );
        throw std::range_error( "Axilliary callback with requested name "
                                "not registered." );
    }
    _callbacks.emplace( cllbIt->first, cllbIt->second );
}

void
AuxInfoSet::apply( G4GDMLParser & parser,
                   G4LogicalVolumeStore * volStore,
                   std::ostream * warnStreamPtr ) {
    if( !volStore ) {
        volStore = G4LogicalVolumeStore::GetInstance();
    }
    if( !warnStreamPtr ) {
        warnStreamPtr = &(std::cerr);
    }
    // iterate among all known logical volumes
    for( auto lvciter  = volStore->begin();
              lvciter != volStore->end();
              lvciter++ ) {
        G4GDMLAuxListType auxInfo =
                                parser.GetVolumeAuxiliaryInformation(*lvciter);
        G4LogicalVolume * const tVol = *lvciter;
        for( auto ipair : auxInfo ) {
                const G4String & aiType  = ipair.type,
                                 aiValue = ipair.value;
            auto dictPairIt = _callbacks.find( aiType );
            if( _callbacks.end() == dictPairIt ) {
                *warnStreamPtr << "Found unknown auxinfo property type tag `"
                               << ipair.type << "' with value `"
                               << ipair.value << "' at volume `"
                               << tVol->GetName() << "'. Skipping it."
                               << std::endl;
                continue;
            } else {
                dictPairIt->second( tVol, ipair.value, *warnStreamPtr );
            }
        }
    }
}

void
AuxInfoSet::register_callback( const std::string & name,
                               PropertyTreatmentCallback cllb ) {
    if( !_index ) {
        _index = new AuxInfoIndex();
    }
    AuxInfoIndex & idx = *_index;
    auto insertionResult = idx.emplace( name, cllb );
    if( !insertionResult.second ) {
        std::cerr << "AuxInfo callback `" << name
                  << "' was attempted to be registered repeatedly. Ignored."
                  << std::endl;
    }
}

void
AuxInfoSet::list_registered_tags( std::ostream & os ) {
    if( !_index || _index->empty() ) {
        os << "  <empty>" << std::endl;
    }
    for( auto ipair : *_index ) {
        os << "  " << ipair.first << " by pointer "
           << ipair.second << std::endl;
    }
}

}  // namespace extGDML

