/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "config.h"

// # include <goo_exception.hpp>
# include <G4VSensitiveDetector.hh>

# include "SensDetDict.hpp"

namespace extGDML {

SDDictionary * SDDictionary::_self = nullptr;

SDDictionary &
SDDictionary::self(){
    if( !_self ) {
        _self = new SDDictionary();
    }
    return *_self;
}

SDDictionary::~SDDictionary() {}

void
SDDictionary::register_SD_inststance(
            const std::string & name,
            SDCtr sdPtr ) {
    _dict[name] = sdPtr;
}

SDDictionary::SDCtr
SDDictionary::operator[]( const std::string & name ) const {
    auto it = _dict.find( name );
    if( _dict.end() == it ) {
        //  TODO standalone exceptions for extGDML
        # if 0
        emraise( noSuchKey,
                 "Couldn't find a SensitiveDetector constructor named \"%s\".",
                 name.c_str() );
        # else
            throw
                std::runtime_error("Couldn't find a SensitiveDetector constructor");
        # endif
    }
    return it->second;
}

void SDDictionary::print_SD_List() {
    for ( auto it = _dict.begin(); it != _dict.end(); ++it) {
        std::cout << "\t" << it->first << std::endl;
    }
}

}  // namespace extGDML

