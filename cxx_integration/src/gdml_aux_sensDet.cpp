/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "gdml_aux_sensDet.hpp"
# include "SensDetDict.hpp"

# include <regex>
# include <G4LogicalVolume.hh>
# include <G4SDManager.hh>

/**@file gdml_aux_sensDet.cpp
 * @brief A GDML aux-info tag processor for "sensDet" type.
 *
 * Assigns logical volumes to certain Geant4 sensitive detector instance.
 *
 * Basically, value should consist of two parts separated with column ':' sign.
 * E.g.:
 *  ECAL_cell:/na64det/ecal
 * Will refer to SensitiveDetector subclass named 'ECAL_cell' and create an instance
 * named '/na64det/ecal'.
 *
 * TODO: there are functions for substitution based on C++11 regex, that currently unused,
 * though.
 */

static const std::string
    srxSDClass  = "((?:[[:alnum:]_])+)",
    srxVarSubst = "(\\$\\{(?:[[:alnum:]_]+)\\})",
    srxSDName   = "(/(?:[[:alnum:]_/]|" + srxVarSubst + ")+)",
    srxSDID     = "^" + srxSDClass + ":" + srxSDName + "$"
    ;

static const std::regex
    rxSDClass   ( srxSDClass ),
    rxVarSubst  ( srxVarSubst ),
    rxSDName    ( srxSDName ),
    rxSDID      ( srxSDID )
    ;

std::string
substitute_variables( const std::string & orig, std::unordered_map<std::string, std::string> varDict ) {
    std::string res = orig;
    for( auto it = varDict.begin(); varDict.end() != it; ++it ) {
        res = std::regex_replace( res, std::regex("\\$\\{" + it->first + "\\}"), it->second );
    }
    return res;
}


//
// Aux tag processor
//

EXTGDML_AUXINFO_PROCESSOR_BGN( sensDet, volPtr, strVal, wStream ) {
    std::unordered_map<std::string, std::string> variables; {
            // TODO: this variables dictionary can be useful further:
            // ... variables["foo"]     = "__HereWasAFoo__";
            //variables["PROJ"] = STRINGIFY_MACRO_ARG( PACKAGE );
            variables["PROJ"] = "extGDML";
        };
    // Obtain SDMananger sinleton ptr
    G4SDManager * sdm = ::G4SDManager::GetSDMpointer();

    std::string sensitiveDetectorClass,
                sensitiveDetectorName
                ;

    { // Parse sensitive detector declaration.
        std::string toProcess = strVal;
        bool needSubst = false;
        do {
            if( needSubst ) {
                toProcess = substitute_variables( toProcess, variables );
                needSubst = false;
            }
            std::smatch sm;
            if( std::regex_match( toProcess, sm, rxSDID ) ) {
                for( size_t nPiece = 0;
                     nPiece < sm.size();
                     ++nPiece ) {
                    std::ssub_match subMatch = sm[nPiece];
                    std::string piece = subMatch.str();
                    if(piece.empty()) continue;

                    if( std::regex_match( piece, rxSDID ) ) {
                        //std::cout << "Entire matching string: " << piece << std::endl;
                        continue;
                    } else if( std::regex_match( piece, rxVarSubst ) ) {
                        //std::cout << "            - Variable: " << piece << std::endl;
                        needSubst = true;
                        //std::cout << "== BREAK -- RENEW ==" << std::endl;
                    } else if( std::regex_match( piece, rxSDName ) ) {
                        //std::cout << "       - Instance name: " << piece << std::endl;
                        sensitiveDetectorName = piece;
                    } else if( std::regex_match( piece, rxSDClass ) ) {
                        sensitiveDetectorClass = piece;
                    } else {
                        throw std::runtime_error( "Unexpected token got at "
                            "sensitive detector referencing." );
                        //emraise( badState, "Unexpected \"%s\" token in \"%s\" description.",
                        //         piece.c_str(), strVal.c_str()
                        //        );
                    }
                }
            } else {
                // TODO temporary solution. Rework with log system
                std::cout << "Wrong sensitive detector description:"
                    << strVal.c_str() << std::endl;
                //emraise( malformedArguments,
                //         "Wrong sensitive detector description: \"%s\".\n"
                //         "\tFor detailed information, please use flag --g4.sensitiveDetectorsList",
                //         strVal.c_str()
                //       );
                throw std::runtime_error( "Malformed sensitive detector "
                        "reference." );
            }
        } while( needSubst );
    }

    G4VSensitiveDetector * detPtr = sdm->FindSensitiveDetector( sensitiveDetectorName );

    if( !detPtr ) {
        // Instantiate one as it still not exists:
        try {
            detPtr = 
                extGDML::SDDictionary::self()[ sensitiveDetectorClass ]( sensitiveDetectorName );
        } catch( std::range_error & e ) {
            // TODO
            std::cout << "Couldn't find a " << sensitiveDetectorClass.c_str()
                << "sensitive detector constructor. Skipping detector association"
                << std::endl;
            # if 0
            aframe_logw(
            "Couldn't find a \"%s\" sensitive detector constructor. Skipping detector association.\n",
            sensitiveDetectorClass.c_str() );
            detPtr = nullptr;
            # endif
            wStream << "Unable to find \"" << sensitiveDetectorClass
                    << "\" sensitive detector ctr. Won't be associated."
                    << std::endl;
        }
        if( detPtr ) {
            sdm->AddNewDetector( detPtr );
            // TODO
            std::cout << sensitiveDetectorName.c_str() << " instance of " 
                << sensitiveDetectorClass.c_str()
                <<  " sensitive detector class created." << std::endl;
            # if 0
            aframe_log1( "\"" ESC_CLRGREEN "%s" ESC_CLRCLEAR \
                      "\" instance of \"" ESC_CLRCYAN "%s" ESC_CLRCLEAR \
                      "\" sensitive detector class created.\n",
                    sensitiveDetectorName.c_str(),
                    sensitiveDetectorClass.c_str() );
            # endif
        }
    }
    if( detPtr ) {
 // TODO
            std::cout << sensitiveDetectorName.c_str() << " instance of " 
                << sensitiveDetectorClass.c_str()
                <<  " sensitive detector class bound to volume"
                << volPtr->GetName().c_str() << std::endl;
        # if 0
        aframe_log2( "\"" ESC_CLRGREEN "%s" ESC_CLRCLEAR \
              "\" instance of \"" ESC_CLRCYAN "%s" ESC_CLRCLEAR \
              "\" sensitive detector class bound to volume \"" ESC_CLRBOLD "%s" ESC_CLRCLEAR "\".\n",
                sensitiveDetectorName.c_str(),
                sensitiveDetectorClass.c_str(),
                volPtr->GetName().c_str()
                );
        # endif
        volPtr->SetSensitiveDetector( detPtr );
    }
} EXTGDML_AUXINFO_PROCESSOR_END( sensDet )

