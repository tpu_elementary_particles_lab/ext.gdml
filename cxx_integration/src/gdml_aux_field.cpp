/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "gdml_aux_field.hpp"
# include "logging.hpp"
# include "errors.hpp"

# if G4VERSION_NUMBER > 999
    # include <G4AutoDelete.hh>
# endif

# include "FieldDict.hpp"

# include <G4LogicalVolume.hh>
# include <G4FieldManager.hh>
# include <G4Field.hh>
# include <G4MagneticField.hh>

# include <regex>

static const std::string
    srxFieldValueComponent = "-?([[:digit:]]+)(\\.([[:digit:]]+)?)?",
    srxFieldValue          = srxFieldValueComponent + "/" + 
                           srxFieldValueComponent + "/" +
                           srxFieldValueComponent, 
    srxFieldType           = "[[:alnum:]]+",                   
    srxFieldWhole          = srxFieldType + ":" + srxFieldValue;

static const std::regex
    rxFieldValueComponent   ( srxFieldValueComponent ),
    rxFieldValue            ( srxFieldValue ),
    rxFieldType             ( srxFieldType  ),
    rxFieldWhole            ( srxFieldWhole )
    ;
EXTGDML_AUXINFO_PROCESSOR_BGN( field, volPtr, strVal, eStream)
{
    std::string strToProcess = strVal,
                fieldType;
    G4ThreeVector fieldValue; 
    if (std::regex_match( strToProcess, rxFieldWhole )) {
        //aframe_log2("FieldValue -- OK.\n");  // TODO log, errors etc.
        extGDML_info( "Field value - ok (vol \"%s\", %p).\n",
            volPtr->GetName().c_str(), volPtr );
    } else {
        //aframe_logw("Malformed argument <field>.\n");
        EXT_GDML_ERR( invalidExpression,
                      "Field value expression malformed: \"%s\" "
                      "(vol \"%s\", %p).\n",
            strToProcess.c_str(),
            volPtr->GetName().c_str(), volPtr );
    }

    std::smatch sm;

    std::regex_search( strToProcess, sm, rxFieldType);  // Search for Field name
    fieldType = sm[0];
    //std::cout << "magFieldType " << magFieldType << std::endl;

    std::regex_search( strToProcess, sm, rxFieldValue);  // Search for Field value
    strToProcess = sm[0];
    //std::cout << "magValue " << magValue << std::endl;
    for (unsigned i=0; i<3; i++) {
        std::regex_search( strToProcess, sm, rxFieldValueComponent);
        //strToProcess = sm[0];
        fieldValue[i]=std::stod(sm[0]);       
        //std::cout << "Field value " << i << " (" << fieldValue[i] << ") " << std::endl;
        strToProcess=strToProcess.substr(strToProcess.find("/") + 1);
        //std::cout << "magValue now " << magValue << std::endl;
    }

    G4Field * fieldPtr = extGDML::FieldDictionary::self()[fieldType]( fieldValue );
    //p348::MagneticField * MagField = new p348::MagneticField(fieldValue);
    //MagField->field_value(fieldValue);
    G4FieldManager * FieldMan = new G4FieldManager();
    
    FieldMan->SetDetectorField(fieldPtr);  
    if (fieldType=="MagneticField") {  // TODO better checking for magnetic field
        G4MagneticField * magFieldPtr = dynamic_cast<G4MagneticField*>(fieldPtr);    
        FieldMan->CreateChordFinder(magFieldPtr);
    }

    G4bool ToAllDaughters = true;

    volPtr->SetFieldManager(FieldMan, ToAllDaughters);
    if ( volPtr->GetFieldManager() ) {
        extGDML_info( "%s with field value "
                "Fx=%f Fy=%f Fz=%f has been associated with \""
                "%s\" logical volume.\n",
                fieldType.c_str(),
                fieldValue[0], fieldValue[1], fieldValue[2],
                volPtr->GetName().c_str()
                );
    } else { 
        extGDML_warn("No FieldManager was associated with"
            "%s volume.\n", volPtr->GetName().c_str() );
    }
    
    # if G4VERSION_NUMBER > 999
    G4AutoDelete::Register(fieldPtr);
    G4AutoDelete::Register(FieldMan);
    # endif
} EXTGDML_AUXINFO_PROCESSOR_END( field )

