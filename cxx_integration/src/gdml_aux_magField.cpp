/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "gdml_aux_magField.hpp"
# include "logging.hpp"

# if G4VERSION_NUMBER > 999
    # include <G4AutoDelete.hh>
# endif

# include "FieldDict.hpp"

# include <G4LogicalVolume.hh>
# include <G4FieldManager.hh>
# include <G4Field.hh>
# include <G4MagneticField.hh>

# include <regex>

static const std::string
    srxMagValueComponent = "-?([[:digit:]]+)(\\.([[:digit:]]+)?)?",
    srxMagValue          = srxMagValueComponent + "/" + 
                           srxMagValueComponent + "/" +
                           srxMagValueComponent, 
    srxMagType           = "[[:alnum:]]+",                   
    srxMagWhole          = srxMagType + ":" + srxMagValue;

static const std::regex
    rxMagValueComponent   ( srxMagValueComponent ),
    rxMagValue            ( srxMagValue ),
    rxMagType             ( srxMagType  ),
    rxMagWhole            ( srxMagWhole )
    ;

EXTGDML_AUXINFO_PROCESSOR_BGN( magField, volPtr, strVal, eStream)
{
    std::string strToProcess = strVal,
                magFieldType,
                magValue;
    G4ThreeVector fieldValue; 
    //if (std::regex_match( strToProcess, rxMagWhole ))
    //    aframe_log2("MagValue -- OK.\n");  // TODO log, errors etc.
    //else aframe_logw("Malformed argument <magField>.\n");
    if( !std::regex_match( strToProcess, rxMagWhole ) ) {
        eStream << "Malformed argument for magnetic field aux info: \""
                << strToProcess << "\"." << std::endl;
    }

    std::smatch sm;

    std::regex_search( strToProcess, sm, rxMagType);  // Search for Field name
    magFieldType=sm[0];
    //std::cout << "magFieldType " << magFieldType << std::endl;

    std::regex_search( strToProcess, sm, rxMagValue);  // Search for magnetic value
    magValue=sm[0];
    //std::cout << "magValue " << magValue << std::endl;
    for (unsigned i=0; i<3; i++) {
        std::regex_search( magValue, sm, rxMagValueComponent);
        //strToProcess = sm[0];
        fieldValue[i]=std::stod(sm[0]);       
        //std::cout << "Field value " << i << " (" << fieldValue[i] << ") " << std::endl;
        magValue=magValue.substr(magValue.find("/") + 1);
        //std::cout << "magValue now " << magValue << std::endl;
    }

    G4Field * fieldPtr = extGDML::FieldDictionary::self()[magFieldType]( fieldValue );
    //p348::MagneticField * MagField = new p348::MagneticField(fieldValue);
    //MagField->field_value(fieldValue);
    G4FieldManager * FieldMan = new G4FieldManager();
    
    FieldMan->SetDetectorField(fieldPtr);  
    if (magFieldType=="MagneticField") {
        G4MagneticField * magFieldPtr = dynamic_cast<G4MagneticField*>(fieldPtr);    
        FieldMan->CreateChordFinder(magFieldPtr);
    }

    G4bool ToAllDaughters = true;

    volPtr->SetFieldManager(FieldMan, ToAllDaughters);
    if ( volPtr->GetFieldManager() ) {
        extGDML_info( "TODO magField with magnetic field "
                "TODO value of magField has been associated with "
                "\"%s\" logical volume.\n",
                //strVal,
                //fieldValue,
                volPtr->GetName().c_str()
                );
    } else {
        extGDML_warn("No FieldManager was associated with "
            "\"%s\" volume.\n",
            volPtr->GetName().c_str() );
    }
    
    # if G4VERSION_NUMBER > 999
    G4AutoDelete::Register(MagField);
    G4AutoDelete::Register(FieldMan);
    # endif
} EXTGDML_AUXINFO_PROCESSOR_END( magField )

