#!/bin/env python
# -*- coding: utf8 -*-
from __future__ import print_function

"""
Script generating GDML output from templates project.
"""

from extGDML import render_geometry_library
import argparse as AP
import sys, os

#
# Only active at standalone mode:

settingsG = {
    'worldVolume' : {
        #'name' : 'ECAL_air_dedicated_Hull'
        'name' : 'Hall',
        'size' : ['160', '160', '50000', 'mm']
    },
    'detectors' : {
        #'ECAL' : {
        #    'geometryFile' : 'ECAL/ecal_segm_nopaper.gdml',  # 'ECAL/ecal.gdml'
        #    'position' : [ -250, 0, 1800, 'mm' ],
        #    'sensDet' : 'ECAL_cell:/${PROJ}det/ECAL_segm'
        #},
        #'HCAL' : {
        #    'geometryFile' : 'HCAL/HCAL_segm.gdml',
        #    'position' : [ -250, 0, 3500, 'mm' ]
        #},
        #'hodoscope' : {
        #    'geometryFile' : 'hodoscope/hodoscope.tgdml',
        #    'position' : [ -250, 0, 1300, 'mm' ]
        #},
        #'BGO' : {
        #    'geometryFile' : 'BGO/BGO.tgdml',
        #    'position' : [ 0, 0, '550', 'mm' ],
        #    'rotation' : [ 90, 0, 90, 'deg' ]
        #},
        #    'geometryFile' : 'hodoscope/hodoscope.gdml',
        #    'position' : [ -250, 0, 1300, 'mm' ]
        #},
        #'BGO' : {
        #    'geometryFile' : 'BGO/BGO.gdml',
        #    'position' : [ 0, 0, '550', 'mm' ],
        #    'rotation' : [ 90, 0, 90, 'deg' ]
        #},
        'WCAL' : {
            'geometryFile' : 'WCAL/wcal.gdml',
            'position' : [ 0, 0, 0, 'mm' ],
        },
        #'Magnet' : {
        #    'geometryFile' : 'magnets/magnet.gdml'
        #},
        #,
        #'HCAL' : {
        #    'geometryFile' : 'HCAL/HCAL_segm.gdml',
        #    'position' : 'ecal_segm_nopaperZeroedBeginning',
        #    'sensDet' : 'ECAL_cell:/${PROJ}det/HCAL_segm'
        #}
    }
}

if "__main__" == __name__:
    ap = AP.ArgumentParser( description=__doc__, prog=sys.argv[0] )
    ap.add_argument( "indexInputFile" )
    ap.add_argument( "--templates-dir", "-D",
                     default=None,
                     help="Templates directory base path from which every "
                     "include instruction will be performed." )
    ap.add_argument( "--output-dir", "-O",
                     default=None,
                     help="Directory where output will be placed." )
    args = ap.parse_args()

    render_geometry_library( os.path.abspath(args.templates_dir if args.templates_dir
                              else os.path.dirname(os.path.realpath(args.indexInputFile)) ),
                            os.path.abspath( args.indexInputFile ),
                            settingsG,
                            args.output_dir )

    #renderedGDML = render_from_template(
    #        (args.templates_dir if args.templates_dir
    #                          else os.path.dirname(os.path.realpath(args.indexInputFile)) ),
    #        args.indexInputFile )
    #print( renderedGDML )
