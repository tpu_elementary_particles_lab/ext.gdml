#!/bin/env python
# -*- coding: utf8 -*-
from __future__ import print_function

"""
Script performing GDML parsing and converting entities to
TGeo classes, storing them into a ROOT file.
"""

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True  # Get. The. Fuck. Off!

import sys, os, re
import argparse as AP
from extGDML.gdmlFileExport import export_gdml_file
from extGDML.definitionsIndex import resolve_NIST_material
from extGDML.root import SystemOfUnits as ROOTSystemOfUnits
from extGDML.root import open_ROOT_output_file

if "__main__" == __name__:
    ap = AP.ArgumentParser( description=__doc__, prog=sys.argv[0] )
    ap.add_argument( "indexInputFile" )
    ap.add_argument( "--working-dir", "-w",
                     default=None,
                     help="A root directory for GDML files structure." )
    ap.add_argument( "--root-file", "-o",
                     default="/tmp/geometry.root",
                     help="A destination ROOT file where conversion result "
                     "is to be stored." )
    ap.add_argument( "--verbose-parse",
                     action='store_true',
                     help="Print out the parsed GDML." )
    ap.add_argument( "--verbose-convert",
                     action='store_true',
                     help="Print out the conversion log and stats." )
    ap.add_argument( "--NIST-materials",
                     default='NIST-mats.gdml',
                     help="A path to GDML file containing NIST materials "
                     "description." )
    ap.add_argument( "--setup",
                     default='Default',
                     help="A setup name from which top volume will be "
                     "extracted.")
    args = ap.parse_args()

    open_ROOT_output_file( args.root_file )

    idx = export_gdml_file(
                args.indexInputFile,
                verboseParse=args.verbose_parse,
                verboseConvert=args.verbose_convert,
                indexCtrKWArgs={
                    'units' : ROOTSystemOfUnits,
                    'suppMaterialsResolver' : (resolve_NIST_material \
                                    if args.NIST_materials else None)
                    },
                NISTMaterialsFilePath=args.NIST_materials,
                exportFormat='ROOT',
                outFilePath=args.root_file,
                finalizeGeometry=True,
                setupName=args.setup
                )
    print( idx )
    # uncomment this lines to fall back to the interpreter:
    #import code
    #code.interact(local=locals())

    #nistMatsGDML = None
    #if args.NIST_materials:
    #    subIdxNISTmats = idx.new_subindex( 'NISTMat' )
    #    nistMatsGDML, isValid = parse_gdml( args.NIST_materials,
    #                                        silence=args.verbose_parse )
    #    if nistMatsGDML and isValid:
    #        print( "NIST materials read." )
    #    else:
    #        sys.stderr.write( "Trouble occured while reading out NIST mats.\n" )
    #gdml2root( gdml, idx,
    #           outFilePath=args.root_file,
    #           quiet=args.verbose_convert,
    #           NISTMaterialsGDML=nistMatsGDML )
    #print( idx )

