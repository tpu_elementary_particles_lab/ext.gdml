# The ext.GDML's `sVbp/` dir

This directory keeps a module written as
[Flask blueprints](http://flask.pocoo.org/docs/0.12/blueprints/#blueprints)
which is designed to be embedded into sV-resources web applications.

