# A `schema/` dir of extGDML sources tree

This directory contains GDML schema definitions and is used for pythonic parser
generation by `generateDS` package. There are also patches for various versions
of this schema.

The reason why one can need these patches is that native Geant4 parser
introduces some incompatibilities with respect to official schema releases. In
order to make schema support all the Geant4 features we're keeping our schema
modifications in patches in hope that they can eventually be used by GDML
commitee.


## Patched schema version generation

The cmake instructions in `CMakeLists.txt` will download schema archive and
apply patch automatically. Considering current directory being the `schema/`:

    $ mkdir build
    $ cd build
    $ cmake ..
    $ make

The root ext.GDML's `CMakeLists.txt` will use alternative build directory
however.


## Making own patches

TODO: currently, only version of Makefile for deprecated ext.gdml version is
available to be further used as a draft. The text below describes only desired
behaviour for Makefile that is currently not implemented.

Making patches procedure is somehow automated: after performing changes in
`schema/gdml.dev/` directory (which one need to create) one need to invoke
`make patch` in `schema/` dir and rename the `schema.patch` file to
`patch_<VERSION>.patch` where "`{VERSION}`" is the version of current GDML
schema. This file then will be used automatically by `CMakeLists.txt` upon
built agains apropriate version of schema. Note, that content of `schema/gdml`
will be overwriten with the `schema/gdml.dev`.

DO NOT use the files inside the `schema/gdml` for development needs, otherwise
this changes may break the patch applying logic.

