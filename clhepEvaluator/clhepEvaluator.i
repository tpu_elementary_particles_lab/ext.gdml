%module clhepEvaluator

%pythoncode %{
from extGDML.gdmlError import GDMLEvaluatorError, evaltr_status_is_warning, \
        evaluator_warning
%}

%feature("shadow") HepTool::Evaluator::evaluate %{
def evaluate(self, *args, **kwargs):
    rs = $action(self, *args)
    expectedStatus = kwargs.pop( 'expectedStatus', None )
    if (self.status() and expectedStatus is None) or \
       ((not expectedStatus is None) and expectedStatus != self.status() ):
        if evaltr_status_is_warning(self.status()):
            evaluator_warning( self, expectedStatus=expectedStatus )
        else:
            raise GDMLEvaluatorError( self, *args,
                    expr=args[0],
                    details='while evaluating' )
    return rs%}

%feature("shadow") HepTool::Evaluator::setVariable %{
def setVariable(self, *args, **kwargs):
    # TODO: support for optional forceEvaluation
    rs = $action(self, *args)
    expectedStatus = kwargs.pop( 'expectedStatus', None )
    if (self.status() and expectedStatus is None) or \
       ((not expectedStatus is None) and expectedStatus != self.status() ):
        if evaltr_status_is_warning(self.status()):
            evaluator_warning( self, expectedStatus=expectedStatus )
        else:
            raise GDMLEvaluatorError( self, *args,
                    expr=args[1],
                    details='while setting variable "%s"'%args[0] )
    return rs%}

%feature("shadow") HepTool::Evaluator::setFunction %{
def setFunction(self, *args, **kwargs):
    # TODO: support for optional forceEvaluation
    rs = $action(self, *args)
    expectedStatus = kwargs.pop( 'expectedStatus', None )
    if (self.status() and expectedStatus is None) or \
       ((not expectedStatus is None) and expectedStatus != self.status() ):
        if evaltr_status_is_warning(self.status()):
            evaluator_warning( self )
        else:
            raise GDMLEvaluatorError( self, *args,
                    expr=args[1],
                    details='while setting function "%s"'%args[0] )
    return rs%}

%include <std_string.i>

%{
# include "CLHEP/Evaluator/Evaluator.h"
%}

%include "CLHEP/Evaluator/Evaluator.h"

%pythoncode %{
def _evaluate_integer(self, *args, **kwargs):
    res = self.evaluate( *args, **kwargs )
    if not res.is_integer():
        raise ValueError( "%s evaluated not to whole number (%e) while integer "
            "is required."%( str(args), res ) )
    return int(res)

def _unit_conversion(self, val, fromUnit='1', toUnit='1'):
    s = '({val})*({fromUnit})/({toUnit})'.format(**{
        'val' : val, 'fromUnit' : fromUnit, 'toUnit' : toUnit})
    return self.evaluate( s )

Evaluator.eval_int = _evaluate_integer
Evaluator.convert_units = _unit_conversion
%}

